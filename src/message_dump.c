/*
  This file is part of libfints

  libfints is free software; you can redistribute it
  and/or modify it under the terms of the GNU General
  Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any
  later version.

  libfints is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty
  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with libfints; see the file COPYING.  If not,
  If not, see <http://www.gnu.org/license>
*/


#include "libebics.h"

#define LOG(level,...) EBICS_util_log_from (__LINE__,__FILE__,__func__,level, "messagedump",__VA_ARGS__)



int
main (int argc, char **argv)
{
  uint8_t result = 0;
  uint8_t retv;

  EBICS_init_library();

  /* Load all documents we need for generating the messages */
  struct EBICS_genex_document genexList[EBICS_GENEX_MAX_ENTRIES];
  char *genexFiles[EBICS_GENEX_MAX_ENTRIES] = {
    "ebicsRequest.xml", 
    "SignaturePubKeyOrderData.xml", 
    "HIARequestOrderData.xml",
    "ebicsUnsecuredRequest.xml", 
    "ebicsNoPubKeyDigestsRequest.xml",
    NULL};

  /* Load all existing/needed keys from disk. */
  struct EBICS_Key keyList[EBICS_KEY_MAX_ENTRIES];
  char *keyFiles[EBICS_KEY_MAX_ENTRIES] = {
    "userAuthKey",
    "userEncKey",
    "userSigKey",
    "bankAuthKey",
    "bankEncKey",
    "bankSigKey"};

  char *arg_genexpath;
  if (1 >= argc)
  {
    LOG (EBICS_LOGLEVEL_FATAL, "Call this way: %s path_to_genex_file_folder path_to_keyfiles", argv[0]);
    goto cleanup;
  } else
  {
    arg_genexpath = argv[1];
    LOG (EBICS_LOGLEVEL_DEBUG, "exec 'init_genex(%s)'", arg_genexpath);
    retv = EBICS_init_genex_documents (genexList, arg_genexpath, genexFiles);
    if (EBICS_FATAL == retv)
    {
      LOG (EBICS_LOGLEVEL_FATAL,"Could not parse essential documents. Leaving.");
      goto cleanup;
    }
  }

  char *arg_keypath;
  if (2 >= argc)
  {
    LOG (EBICS_LOGLEVEL_FATAL, "Call this way: %s path_to_genex_file_folder path_to_keyfiles", argv[0]);
    goto cleanup;
  } else
  {
    arg_keypath = argv[2];
    LOG (EBICS_LOGLEVEL_DEBUG, "exec 'init_keymaterial(%s)'", arg_keypath);
    retv = EBICS_init_keymaterial (keyList, arg_keypath, keyFiles);
    if (EBICS_FATAL == retv)
    {
      LOG (EBICS_LOGLEVEL_FATAL, "Could not load essential key files. Leaving.");
    }
  }
  struct EBICS_ARGS_build_header headerArgs = {
    .hostID = "EBIXHOST",
    .partnerID = "PARTNER1",
    .userID = "USER0001",
    .productName = "Unknown/Development",
    .languageCode = "de",
    .requestBankSignature = false};

  struct EBICS_ARGS_build_auth authArgs = {
    .userAuthentication = &keyList[0]
  };

  struct EBICS_ARGS_build_content_ini iniArgs = {
    .document = &genexList[1],
    .userAuthKey = &keyList[0],
    .partnerID = headerArgs.partnerID,
    .userID = headerArgs.userID
  };

  struct EBICS_ARGS_build_content_hia hiaArgs = {
    .document = &genexList[2],
    .userEncKey = &keyList[1],
    .userSigKey = &keyList[2],
    .partnerID = "PARTNER-ID",
    .userID = "USER-ID"
  };
  struct EBICS_ARGS_build_content_camt053 camt053Args = {};

  /* unsigned (?) message  */
  retv = EBICS_generate_message_ini (&genexList[3],
                                     &headerArgs,
                                     &iniArgs);
  util_dump_message(&genexList[3]);
  retv = EBICS_free_genex_documents(genexList);


  /* Camt053  */
  retv = EBICS_init_genex_documents (genexList,
                                     arg_genexpath,
                                     genexFiles);
  retv = EBICS_generate_message_camt053 (&genexList[0],
                                         &headerArgs,
                                         &authArgs,
                                         &camt053Args);
  util_dump_message(&genexList[0]);
  retv = EBICS_free_genex_documents(genexList);


  /* Hia  */
  retv = EBICS_init_genex_documents (genexList,
                                     arg_genexpath,
                                     genexFiles);

  retv = EBICS_generate_message_hia (&genexList[3], // 'document'
                                     &headerArgs,
                                     &hiaArgs); // hiaArgs->document does exist too.
  util_dump_message(&genexList[3]);
  retv = EBICS_free_genex_documents(genexList);

  /* Hpb */
  retv = EBICS_init_genex_documents (genexList,
                                     arg_genexpath,
                                     genexFiles);
  retv = EBICS_generate_message_hpb (&genexList[4],
                                     &headerArgs,
                                     &authArgs); 
  util_dump_message(&genexList[4]);


cleanup:
  retv = EBICS_free_genex_documents(genexList);
  retv = EBICS_free_keymaterial(keyList);
  retv = EBICS_close_library();

  return(result);
}
