/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/


#include <stdio.h>
#include <curl/curl.h>
 
int main(void)
{

  char *data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ebics:ebicsUnsecuredRequest xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ebics=\"urn:org:ebics:H004\" xmlns=\"http://www.w3.org/2001/XMLSchema\" Version=\"PLACEHOLDER\" Revision=\"PLACEHOLDER\"><ebics:header authenticate=\"true\"><ebics:static><ebics:HostID>EBIXHOST</ebics:HostID><ebics:PartnerID>PARTNER1</ebics:PartnerID><ebics:UserID>USER0001</ebics:UserID><ebics:Product Language=\"de\" InstituteID=\"PLACEHOLDER\">Unknown/Development</ebics:Product><ebics:OrderDetails><ebics:OrderType>INI</ebics:OrderType><ebics:OrderAttribute>DZHNN</ebics:OrderAttribute></ebics:OrderDetails><ebics:SecurityMedium>0000</ebics:SecurityMedium></ebics:static><ebics:mutable/></ebics:header><ebics:body><ebics:DataTransfer><ebics:OrderData>eJx1k1lzokAUhd/zK1LOo5XQiAtaSorYEhEB2ZT41kDLouwNLr9+jJrUmGQe7zlf3+669/Tw5RDvHmtclGGajBr0M2g84sRNvTDxR42KbJ7Yxgv3MMRl6A+M0E8QqQq8qBwJH9XCwwVEBD2eeyTl4IMZNQJCsgFF7ff7Z+yEbvmcFj5lAEA3bphX3kF75kK0AAAU6FNnxjv3+XOj/4PSlC3PDTfAMWpwvz5OTDbpzbkKS7SrMDf0yoFu8He1nHrVrio5XuNfR0PqH+XDnRyyNMEJ4XiZJU07muorSahgM0hKZ8K4mRcTYSmWEYUpaObICS2gyCgSOrPM8frzqbY4jWtzowgGMSImYwQ0LrqEWfly4PCC76dKsLTV46x+fT+s6aTTnnmtyqCrJlxJnWyubyhZ8w+agEoZoEmc9mNVM9V91cvPJx2oiFu27m2VyF0wgYMtswZ+2IEILvrRligUI54UceXKirUN5wosi5PDM/l7r5lnWPD53O02l2ofaeOTF8G1EUPwlnm1XetSpauxZb+J+ckN5LVSB3nePB4sADBpkTTe9I+RjUTllO8k1VMkyJOeOBVsdmc4sN5vqqnU1Zbs2Jr30nFIUq1N5sduOmN7MRVlcOGLbK6aO2t0HfnXmC/V3YouSzTDGBsExRnXAnTnCbBPrZZJMwOmPWh310PqG3QT7lZ/H5PlNfMcD0DnRv/wvus/o4UKkuBChNyC101lotOfF38ZV9AqL4VlTPRz0D+pm/r7NV/fi3v4Cwl4P4k==</ebics:OrderData></ebics:DataTransfer></ebics:body></ebics:ebicsUnsecuredRequest>";


  CURL *curl;
  CURLcode res;

  curl_global_init(CURL_GLOBAL_ALL);

  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://127.0.0.1:8080");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    struct curl_slist *headers=NULL;
     headers = curl_slist_append(headers, "Content-Type: text/xml");
      
     /* post binary data */
     curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

     /* pass our list of custom made headers */
     curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

  
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
 
    /* always cleanup */ 
    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);
    curl_global_cleanup();
  }
  return 0;
}
