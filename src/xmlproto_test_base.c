/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/

#include "xmlmessages.h"
#include "libxml/xpointer.h"
#include <assert.h>
#include <stdio.h>
#include "util.h"
#include <fts.h>

#define LOG(level,...) EBICS_util_log_from (__LINE__,__FILE__,__func__,level, "xmlproto_test_base",__VA_ARGS__)

#define EBICS_GENEX_MAX_ENTRIES 64
#define EBICS_GENEX_MAX_NAME 128

int
main(int argc, char **argv)
{

  if (argc < 2)
  {
    printf("Parameters insufficient\n");
    return(1);
  }

  /* Open all genex files we need to use*/

  xmlInitParser();

  char *genex_schemas_dir = argv[1];
  char *genexfiles[] = {"ebicsRequest.xml", "SignaturePubKeyOrderData.xml", "HIARequestOrderData.xml"};
  size_t genexlistsize = sizeof(genexfiles)/sizeof(char*);
  struct EBICS_genex_document genexlist[genexlistsize];
  LOG (EBICS_LOGLEVEL_DEBUG,"Size of genexfiles: %u", genexlistsize);
  for (int i = 0; i < genexlistsize; i++)
  {
    size_t size = snprintf (NULL, 0,"%s/%s", genex_schemas_dir, genexfiles[i]) + 1;
    assert(0 != size);
    char filepath[size];
    size = snprintf (filepath, size,"%s/%s", genex_schemas_dir, genexfiles[i]);
    assert(0 != size);

    LOG (EBICS_LOGLEVEL_DEBUG, "Loading file %s", filepath);
    struct EBICS_genex_document *genex = &genexlist[i];

    xmlParserCtxtPtr parser = xmlNewParserCtxt();
    xmlDocPtr doc = xmlCtxtReadFile(parser, filepath, "utf-8", 0);
    assert(NULL != doc);
    xmlXPathContextPtr xpathCtxPtr = xmlXPathNewContext(doc);
    assert(NULL != xpathCtxPtr);
    xmlXPathRegisterNs(xpathCtxPtr, "ebics", "urn:org:ebics:H004");
    xmlXPathRegisterNs(xpathCtxPtr, "schema", "http://www.w3.org/2001/XMLSchema");
    xmlXPathRegisterNs(xpathCtxPtr, "ds", "http://www.w3.org/2000/09/xmldsig#");
    genex->document = doc;
    genex->parser = parser;
    genex->xpath = xpathCtxPtr;
    strncpy (genex->name, genexfiles[i], EBICS_GENEX_MAX_NAME);
    LOG (EBICS_LOGLEVEL_DEBUG, "I: %d -- name: %s, genex: %p, xpath: %p, doc: %p", i, genex->name, genex, xpathCtxPtr, doc);
  }

  
  xmlDocPtr doc = genexlist[0].document;

  /* Initialize libgcrypt */

  /* Version check should be the very first call because it
     makes sure that important subsystems are intialized. */
  if (!gcry_check_version (GCRYPT_VERSION))
  {
    fputs ("libgcrypt version mismatch\n", stderr);
    exit (2);
  }

  /* Disable secure memory.  */
  gcry_control (GCRYCTL_DISABLE_SECMEM, 0);

  /* ... If required, other initialization goes here.  */

  /* Tell Libgcrypt that initialization has completed. */
  gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);

  /* End of libgcrypt initialization */


#ifndef XMLSEC_NO_XSLT
  xsltSecurityPrefsPtr xsltSecPrefs = NULL;
#endif /* XMLSEC_NO_XSLT */

/* Init libxml and libxslt libraries */
    LIBXML_TEST_VERSION
    xmlLoadExtDtdDefaultValue = XML_DETECT_IDS | XML_COMPLETE_ATTRS;
    xmlSubstituteEntitiesDefault(1);
#ifndef XMLSEC_NO_XSLT
    xmlIndentTreeOutput = 1; 
#endif /* XMLSEC_NO_XSLT */

    /* Init libxslt */
#ifndef XMLSEC_NO_XSLT
    /* disable everything */
    xsltSecPrefs = xsltNewSecurityPrefs(); 
    xsltSetSecurityPrefs(xsltSecPrefs,  XSLT_SECPREF_READ_FILE,        xsltSecurityForbid);
    xsltSetSecurityPrefs(xsltSecPrefs,  XSLT_SECPREF_WRITE_FILE,       xsltSecurityForbid);
    xsltSetSecurityPrefs(xsltSecPrefs,  XSLT_SECPREF_CREATE_DIRECTORY, xsltSecurityForbid);
    xsltSetSecurityPrefs(xsltSecPrefs,  XSLT_SECPREF_READ_NETWORK,     xsltSecurityForbid);
    xsltSetSecurityPrefs(xsltSecPrefs,  XSLT_SECPREF_WRITE_NETWORK,    xsltSecurityForbid);
    xsltSetDefaultSecurityPrefs(xsltSecPrefs); 
#endif /* XMLSEC_NO_XSLT */
                
    /* Init xmlsec library */
    if(xmlSecInit() < 0) {
        fprintf(stderr, "Error: xmlsec initialization failed.\n");
        return(-1);
    }

    /* Check loaded library version */
    if(xmlSecCheckVersion() != 1) {
        fprintf(stderr, "Error: loaded xmlsec library version is not compatible.\n");
        return(-1);
    }

    /* Init crypto library */
    if(xmlSecCryptoAppInit(NULL) < 0) {
        fprintf(stderr, "Error: crypto initialization failed.\n");
        return(-1);
    }

    /* Init xmlsec-crypto library */
    if(xmlSecCryptoInit() < 0) {
        fprintf(stderr, "Error: xmlsec-crypto initialization failed.\n");
        return(-1);
    }

  struct EBICS_ARGS_build_part_header headerArgs = {
    .hostID = "EBIXHOST",
    .partnerID = "PARTNER1",
    .userID = "USER0001",
    .productName = "Unknown/Development",
    .languageCode = "de",
    .requestBankSignature = false};

  struct EBICS_ARGS_build_part_camt053 camt053Args = {
    .startdate = {
      .day = 12,
      .month = 4,
      .year = 1991},
    .enddate = {
      .day = 21,
      .month = 9,
      .year = 2015}
  };

  if (argc < 3) {
    printf("Keyfile missing at argpos 2\n");
    return(-1);
  }

  char *key_file = argv[2];
  int size = 0;
  FILE *f = fopen(key_file, "rb");
  fseek(f, 0, SEEK_END);
  size = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *result = (char *)malloc(size+1);
  size_t res = fread(result, sizeof(char), size, f);
  assert(0 != res);
  fclose(f);
  result[size] = 0;

  if (!gcry_control (GCRYCTL_INITIALIZATION_FINISHED_P))
  {
    LOG(EBICS_LOGLEVEL_ERROR,"libgcrypt has not been initialized");
    //TODO: if not done initialize libgcrypt inside library?
  }

/*
  struct EBICS_ARGS_build_part_auth authArgs = {
  .bankAuthentication = {
    .type = EBICS_KEY_RSA_PUBLIC,
    .publickey.algorithm = EBICS_KEY_ALGORITHM_SHA256,
    .publickey.data = "1H/rQr2Axe9hYTV2n/tCp+3UIQQ="
  },
  .bankEncryption = {
    .type = EBICS_KEY_RSA_PUBLIC,
    .publickey.algorithm = EBICS_KEY_ALGORITHM_SHA256,
    .publickey.data = "2joEROI3092OIFP394+WOIer2WI="
  },
  .userAuthentication = {
    .type = EBICS_KEY_RSA_PRIVATE,
    .privatekey.algorithm = EBICS_KEY_ALGORITHM_SHA256,
    .privatekey.len = (size_t)size,
    .privatekey.data = result
  }
  //.userEncryption = NULL,
  //.userSignature = NULL,
};

  struct EBICS_MSG_Spec foo[] = {
    EBICS_MSG_op_subcommand(EBICS_build_part_header, &headerArgs,NULL),
    EBICS_MSG_op_subcommand(EBICS_build_part_camt053, &camt053Args, NULL),
    EBICS_MSG_op_subcommand(EBICS_build_part_auth ,&authArgs, NULL),
    EBICS_MSG_op_clean(),
    EBICS_MSG_op_end()
  };


  struct EBICS_genex_document *document = &genexlist[0];
  LOG (EBICS_LOGLEVEL_DEBUG, "genex: %p, xpath: %p, doc: %p", document, document->xpath, document->document);
  EBICS_MSG_parse_spec(foo, document);
*/

  // load all needed files

  // generate message_spec
  // parse message spec ( message_spec, genex_list )
  // 


  { 
    /* DEBUG
       Dump the document */
    int buffersize;
    xmlChar* xmlbuf;
    xmlThrDefIndentTreeOutput(1024);

    xmlDocDumpFormatMemoryEnc(doc, &xmlbuf, &buffersize, "UTF-8", 1);
    printf("%s\n",xmlbuf);
    xmlFree(xmlbuf);
  }

done:    
  

  free(result);


  //xmlXPathRegisteredNsCleanup(xpathCtxPtr);
  //xmlXPathFreeContext(xpathCtxPtr);


  //xmlFreeParserCtxt(parserCtxPtr);
  //xmlFreeDoc(doc);

  xmlSecOpenSSLShutdown();
  xmlSecOpenSSLAppShutdown();
  /* Shutdown xmlsec-crypto library */
  xmlSecCryptoShutdown();

  /* Shutdown crypto library */
  xmlSecCryptoAppShutdown();

  /* Shutdown xmlsec library */
  xmlSecShutdown();

  /* Shutdown libxslt/libxml */
#ifndef XMLSEC_NO_XSLT
  xsltCleanupGlobals();
#endif /* XMLSEC_NO_XSLT */
  xmlCleanupParser();
  /* cleanup */

}
