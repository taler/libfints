/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/

#ifndef EBICS_XML_UTIL_H
#define EBICS_XML_UTIL_H

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>

#include <libxml/globals.h>
#include <libxml/tree.h>

#include "xmlproto.h"

/**
 * How many characters can a date/time string
 * be at most?
 */
#define DATE_STR_SIZE 64

/**
 * How many characters can a nonce string
 * be at most?
 */
#define NONCE_STR_SIZE 64

/**
 * Loglevels with different behaviour, formating and verbosity of log messages.
 */
#define EBICS_LOGLEVEL_NONE   -1
#define EBICS_LOGLEVEL_FATAL   0
#define EBICS_LOGLEVEL_ERROR   1 
#define EBICS_LOGLEVEL_WARNING 2 
#define EBICS_LOGLEVEL_INFO    3
#define EBICS_LOGLEVEL_DEBUG   4

/**
 * central logging function
 */
void EBICS_util_log_from (int32_t fileline, const char *filename, const char *funcname, const uint8_t level, const char *component, const char *message, ...);

/**
 * Typedef for logging target functions.
 *
 * @param level what is the level of the message (severity)
 * @param component component the message originates from
 * @param date current date and time
 * @param message the actual message
 */
typedef void (*util_logging_target_function_pointer) (uint8_t level, const char *component, const char *date, const char *message);

/**
 * struct with config for logging mechanism
 */
struct util_logging_config
{

  /**
   * Routine for the very final print.
   */
  util_logging_target_function_pointer target;

  /**
   * Default loglevel.
   */
  uint8_t loglevel;
};

/*
 * Base64 encoding from GNUnet
 */
size_t
EBICS_UTIL_base64_encode (const char *data,
                              size_t len,
                              char **output);

/*
 * Base64 decoding from GNUNET
size_t
EBICS_UTIL_base64_decode (const char *data,
                              size_t len, char **output)

*/


void
util_dump_message ( struct EBICS_genex_document *document);

struct EBICS_genex_document*
util_genex_document_from_file ( char *path );

void
util_genex_free_document (struct EBICS_genex_document *document);

xmlChar*
util_test_xpath_value(const char *expression, xmlXPathContextPtr xpath);

char*
util_generate_path(const char *basePath, const char *fileName);



#endif
