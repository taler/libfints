/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/

#ifndef EBICS_LIBEBICS_H
#define EBICS_LIBEBICS_H

#include "util.h"
#include "xmlmessages.h"
#include "libxml/xpointer.h"
#include <gnutls/gnutls.h>
#include <gnutls/abstract.h>
#include <gnutls/crypto.h>
#include <gnutls/x509.h>
#include <assert.h>
#include <stdio.h>
#include <fts.h>
#include <zlib.h>

#define DEBUG EBICS_LOGLEVEL_DEBUG

#define EBICS_GENEX_MAX_ENTRIES 64

#define EBICS_USER_KEYS_NUMBER 3
#define EBICS_KEY_MAX_ENTRIES 6
#define EBICS_KEY_MAX_NAME 128


/**
 * INI & HIA share the same schema.
 */
#define EBICS_INI_WRAPPER_TEMPLATE 3
#define EBICS_INI_PAYLOAD_TEMPLATE 1
#define EBICS_HIA_WRAPPER_TEMPLATE 3
#define EBICS_HIA_PAYLOAD_TEMPLATE 2


/**
 * This struct forces the system to assign the
 * indices (in the global array of keys) to the
 * filename used to import such key.
 */
struct EBICS_UserKeyFiles {

  /**
   * Points to the (as per EBICS terminology) "bank-technical
   * public key".  This is the key used to produce "ES", namely
   * Electronic Signatures of payloads.
   */
  #define EBICS_USER_ES_KEY 0
  char *es_key;
  
  /**
   * Key with which the customer will encrypt their messages.
   */
  #define EBICS_USER_ENC_KEY 1
  char *enc_key;
  
  /**
   * Key used by the customer to identify and authenticate.
   */
  #define EBICS_USER_SIG_KEY 2
  char *sig_key;
};

/**
 * Initializes Libebics.  Init all the dependencies,
 * as well as it allocates the "genex" templates to
 * be copied and instantiated during the library life.
 *
 * @param key_dir directory where keys to be imported
 *        are located.
 * @param key_files array of filenames indicating PEM
 *        formatted files on disk; last entry must be NULL.
 * @return EBICS_SUCCESS or EBICS_ERROR.
 */
int
EBICS_init_library (const char *key_dir,
                    const struct EBICS_UserKeyFiles *key_files);


/**
 * Deinit / deallocate libebics and all the dependencies.
 *
 * @return EBICS_SUCCESS or EBICS_ERROR
 */
int
EBICS_close_library ();

/**
 * Customize a bunch of standard values in the tree (including
 * the HostID).
 *
 * @param headerArgs contains values for the "header" sub-tree.
 * @param header_args TODO
 * @param ini_args TODO
 * @return pointer to a freshly allocated document, NULL upon errors.
 */
struct EBICS_genex_document *
EBICS_generate_message_ini (struct EBICS_ARGS_build_header *header_args,
                            struct EBICS_ARGS_build_content_ini *ini_args);

/**
 * Generator of HIA messages.
 *
 * @param header_args TODO
 * @param hia_args TODO
 * @return pointer to a freshly allocated document, NULL upon errors.
 */
struct EBICS_genex_document *
EBICS_generate_message_hia (struct EBICS_ARGS_build_header *header_args,
                            struct EBICS_ARGS_build_content_hia *hia_args);

/**
 * Generator of HPB messages.
 *
 * @param header_args TODO
 * @param auth_args TODO
 * @return pointer to a freshly allocated document, NULL upon errors.
 */
struct EBICS_genex_document *
EBICS_generate_message_hpb (struct EBICS_ARGS_build_header *header_args,
                            struct EBICS_ARGS_build_auth *auth_args);

/**
 * Generator of CAMT.053 messages.
 *
 * @param header_args TODO
 * @param auth_args TODO
 * @param camt053_args TODO
 * @return pointer to a freshly allocated document, NULL upon errors.
 */
struct EBICS_genex_document *
EBICS_generate_message_camt053 (struct EBICS_ARGS_build_header *header_args,
                                struct EBICS_ARGS_build_auth *auth_args,
                                struct EBICS_ARGS_build_content_camt053 *camt053_args);
#endif
