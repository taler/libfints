/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/

#ifndef EBICS_XML_MESSAGES_H
#define EBICS_XML_MESSAGES_H

#include "util.h"
#include "xmlproto.h"
#include <xmlsec/xmlsec.h>
#include <xmlsec/xmltree.h>
#include <xmlsec/xmldsig.h>
#include <xmlsec/templates.h>
#include <xmlsec/crypto.h>
#include <assert.h>
#include <gcrypt.h>
#include <zlib.h>

/**
 * Struct for build_content_hia subcommand.  This
 * object could be unified with its INI counterpart;
 * just keeping around for a while.
 */
struct EBICS_ARGS_build_content_hia
{

  /**
   * Partner id. See your banks ebics login information
   * (probably recieved via (snail) mail.
   */
  const char *partnerID;

  /**
   * User id. See your banks ebics login information
   * (probably recieved via (snail) mail.
   */
  const char *userID;
};

/* Struct for build_content_ini subcommand.
 */
struct EBICS_ARGS_build_content_ini
{
  /* Document containing the "SignaturePubKeyOrderData" definition. */
  struct EBICS_genex_document *document;
  /* Key containing the authentication key of the user. */
  const struct EBICS_Key *userAuthKey;
  /* partner id. See your banks ebics login information (probably recieved via (snail) mail. */
  const char *partnerID;
  /* user id. See your banks ebics login information (probably recieved via (snail) mail. */
  const char *userID;
};

/* Struct for all build_header_* subcommands
 */
struct EBICS_ARGS_build_header
{
  /* host id. See your banks ebics login information (probably recieved via (snail) mail.*/
  const char *hostID;
  /* partner id. See your banks ebics login information (probably recieved via (snail) mail.*/
  const char *partnerID;
  /* user id. See your banks ebics login information (probably recieved via (snail) mail.*/
  const char *userID;
  /* Name of the product using the library. */
  const char *productName;
  /* ISO-(TODO) 2-letter language code of the application using the library. */
  const char *languageCode;
  /* Set to request the bank to sign the reply (not yet available in EBICS H004). */
  uint8_t requestBankSignature;
};

/* Struct for "camt053" request. */
struct EBICS_ARGS_build_content_camt053
{
  /* Starting date */
  const struct EBICS_Date startdate;
  /* End Date */
  const struct EBICS_Date enddate;
};

/* Struct for all subcommands that need access to user or bank keys. */
struct EBICS_ARGS_build_auth
{
  /* Bank key hashes */
  /* Authentication hash key from bank */
  const struct EBICS_Key *bankAuthentication;
  /* Encryption key hash from bank */
  const struct EBICS_Key *bankEncryption;
  /* Signature key hash from bank
   * TODO: currently only planned but not yet enforced by the standard */
  const struct EBICS_Key *bankSignature;

  /* User keys */
  /* Authentication key(pair) from user */
  const struct EBICS_Key *userAuthentication;
  /* Encryption key(pair) from user */
  const struct EBICS_Key *userEncryption;
  /* Signature key(pair) from user */
  const struct EBICS_Key *userSignature;
};

/* Generate a "ebicsNoPubKeyDigestsRequest" style header.
 *
 * @param cls Pointer to a EBICS_ARGS_build_header struct.
 * @param document Pointer to a EBICS_genex_document containing the "ebicsNoPubKeyDigestsRequest" genex definition.
 *
 */
void
EBICS_build_header_ebicsNoPubKeyDigestsRequest (void *cls, struct EBICS_genex_document *document);

/* Generate a "ebicsUnsecuredRequest" style header.
 *
 * @param cls Pointer to a EBICS_ARGS_part_header struct.
 * @param document Pointer to a EBICS_genex_document containing the "ebicsUnsecuredRequest" genex definition.
 *
 */
void
EBICS_build_header_ebicsUnsecuredRequest (void *cls, struct EBICS_genex_document *document);

/* Generate a "ebicsRequest" style header.
 *
 * @param cls Pointer to a EBICS_ARGS_build_header struct.
 * @param document Pointer to a EBICS_genex_document containing the "ebicsRequest" genex definition.
 *
 */
void
EBICS_build_header_ebicsRequest (void *cls, struct EBICS_genex_document *document);

/* Generate the content for a "INI" request.
 *
 * @param cls Pointer to a EBICS_ARGS_build_content_ini struct.
 * @param document Pointer to a EBICS_genex_document containing the "ebicsUnsecuredRequest" definition.
 *
 */
void
EBICS_build_content_ini (void *cls, struct EBICS_genex_document *document);

/* Generate the content for a "HIA" request.
 *
 * @param cls Pointer to a EBICS_ARGS_build_content_hia struct.
 * @param document Pointer to a EBICS_genex_document containing the "ebicsUnsecuredRequest" definition.
 */
void
EBICS_build_content_hia (void *cls, struct EBICS_genex_document *document);

/* Generate the content for a "C53" request.
 * "C53" requests bank statements for booked transactions between the dates supplied via the args struct cls.
 *
 * @param cls Pointer to a EBICS_ARGS_build_content_hia struct.
 * @param document Pointer to a EBICS_genex_document containing the "ebicsUnsecuredRequest" definition.
 */
void
EBICS_build_content_camt053 (void *cls, struct EBICS_genex_document *document);

/* Generate the content and structures for the bank public key hashes.
 *
 * @param cls Pointer to a EBICS_ARGS_build_auth struct.
 * @param document Pointer to a EBICS_genex_document containing the "ebicsRequest" definition.
 */
void
EBICS_build_bankPubKeyDigest (void *cls, struct EBICS_genex_document *document);

/* Sign ebicsRequest requests.
 *
 * @param cls Pointer to a EBICS_ARGS_build_auth struct.
 * @param document Pointer to a EBICS_genex_document containing the "ebicsRequest" definition.
 */
void
EBICS_build_auth_signature (void *cls, struct EBICS_genex_document *document);

void
EBICS_sign_message(const struct EBICS_Key *bankAuthentication, 
                   const struct EBICS_Key *bankEncryption, 
                   const struct EBICS_Key *userAuthentication, 
                   struct EBICS_genex_document *document);


#endif
