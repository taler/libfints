/**
 * This file is part of libebics
 *
 * libfints is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3,
 * or (at your option) any later version.
 *
 * libebics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with libebics; see the file COPYING.
 * If not, see <http://www.gnu.org/license>
 */

#include "util.h"

#define LOG(level,...) EBICS_util_log_from (__LINE__,__FILE__,__func__,level, "utils",__VA_ARGS__)

/**
 * Local logging target using printf and simple message style.
 *
 * @param level what is the level of the message (severity)
 * @param component component the message originates from
 * @param date current date and time
 * @param message the actual message
 */
static void
myprint (uint8_t level,
         const char *component,
         const char *date,
         const char *message)
{
  char *errornames[8] = {"FATAL",
                         "ERROR",
                         "WARNING",
                         "INFO",
                         "DEBUG"};
  printf ("%s: %s: %s: %s\n",
          date,
          component,
          errornames[level],
          message);
}


/**
 * Example for default global logging config.
 */
struct util_logging_config global_config = {
  .target = myprint,
  1};


/**
 * Output a log message using the registered logging function
 *
 * @param level what is the level of the message
 * @param component component the message originates from
 * @param message the message itself (format string)
 * @param ... the arguments to the format string
 */
void
EBICS_util_log_from (int32_t fileline,
                     const char *filename,
                     const char *funcname,
                     const uint8_t level,
                     const char *component,
                     const char *message,
                     ...)
{

  if (EBICS_LOGLEVEL_NONE > global_config.loglevel)
    return;

  char date[DATE_STR_SIZE];
  size_t content_size;
  size_t wrapper_size;
  time_t timeval;
  struct tm *tmptr;
  va_list vacp;
  va_list va;

  va_start (va, message);
  va_copy (vacp, va);

  content_size = vsnprintf (NULL,
                            0,
                            message,
                            va);
  va_end (va);

  wrapper_size = snprintf (NULL,
                           0,
                           "%s:%d: %s: %s",
                           filename,
                           fileline,
                           funcname,
                           "");

  assert ((0 != content_size)
    && (0 != wrapper_size));

  char content[content_size];
  char wrapper[wrapper_size];

  /* user-provided message.  */
  vsnprintf (content,
             sizeof (content),
             message,
             vacp);

  va_end (vacp);

  if ( (level == EBICS_LOGLEVEL_FATAL) ||
      (EBICS_LOGLEVEL_DEBUG <= global_config.loglevel) )
    snprintf (wrapper,
              /* size accounts for both 0-terminators */
              wrapper_size + content_size - 1,
              "%s:%d: %s: %s",
              filename,
              fileline,
              funcname,
              content);
  else
    snprintf (wrapper,
              content_size,
              "%s",
              content);

  time (&timeval);
  tmptr = localtime (&timeval);

  memset (date,
          0,
          DATE_STR_SIZE);

  strftime (date,
            DATE_STR_SIZE,
            "%b %d %H:%M:%S",
            tmptr);

  global_config.target (level,
                        component,
                        date,
                        wrapper);
}


/** ******************** Base64 encoding ***********/

#define FILLCHAR '='
static char *cvt =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ" "abcdefghijklmnopqrstuvwxyz" "0123456789+/";


/**
 * Encode into Base64.
 *
 * @param data the data to encode
 * @param len the length of the input
 * @param output where to write the output (*output should be NULL,
 *   is allocated)
 * @return the size of the output
 */
size_t
EBICS_UTIL_base64_encode (const char *data,
                          size_t len,
                          char **output)
{
  size_t i;
  char c;
  size_t ret;
  char *opt;

  ret = 0;
  opt = malloc (2 + (len * 4 / 3) + 8);
  *output = opt;
  for (i = 0; i < len; ++i)
  {
    c = (data[i] >> 2) & 0x3f;
    opt[ret++] = cvt[(int) c];
    c = (data[i] << 4) & 0x3f;
    if (++i < len)
      c |= (data[i] >> 4) & 0x0f;
    opt[ret++] = cvt[(int) c];
    if (i < len)
    {
      c = (data[i] << 2) & 0x3f;
      if (++i < len)
        c |= (data[i] >> 6) & 0x03;
      opt[ret++] = cvt[(int) c];
    }
    else
    {
      ++i;
      opt[ret++] = FILLCHAR;
    }
    if (i < len)
    {
      c = data[i] & 0x3f;
      opt[ret++] = cvt[(int) c];
    }
    else
    {
      opt[ret++] = FILLCHAR;
    }
  }
  opt[ret++] = FILLCHAR;
  return ret;
}

/**
 * Dump a EBICS_genex_document to LOG_DEBUG.
 *
 * @param document the document to dump.
 */
void
util_dump_message (struct EBICS_genex_document *document)
{ 
  int buffersize;
  xmlChar* xmlbuf;

  xmlDocDumpFormatMemoryEnc (document->document,
                             &xmlbuf,
                             &buffersize,
                             "UTF-8",
                             1);
  LOG (EBICS_LOGLEVEL_DEBUG,
       "Dumping Document: INI Message\n%s",
       xmlbuf);
  xmlFree (xmlbuf);
}

struct EBICS_genex_document*
util_genex_document_from_file ( char *path )
{
  
  struct EBICS_genex_document *document = malloc(sizeof(struct EBICS_genex_document));

  xmlParserCtxtPtr parser = xmlNewParserCtxt();
  assert(NULL != parser);
  xmlDocPtr doc;
  doc = xmlCtxtReadFile(parser, path, "utf-8", XML_PARSE_NOBLANKS);
  assert(NULL != doc);

  xmlXPathContextPtr xpathCtxPtr = xmlXPathNewContext(doc);
  assert(NULL != xpathCtxPtr);

  document->document = doc;
  document->parser = parser;
  document->xpath = xpathCtxPtr;
  return document;
}

void
util_genex_free_document (struct EBICS_genex_document *document)
{
  xmlXPathRegisteredNsCleanup(document->xpath);
  xmlXPathFreeContext(document->xpath);
  xmlFreeParserCtxt(document->parser);
  xmlFreeDoc(document->document);

  free(document);
}

xmlChar*
util_test_xpath_value(const char *expression, xmlXPathContextPtr xpath)
{
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression(BAD_CAST expression, xpath);
  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  assert(0 < xpathObjPtr->nodesetval->nodeNr);
  xmlChar *nodeText = xmlNodeGetContent(xpathObjPtr->nodesetval->nodeTab[0]);
  xmlXPathFreeObject(xpathObjPtr);
  return nodeText;
}

char*
util_generate_path(const char *basePath, const char *fileName)
{
  size_t strLength;
  strLength = strlen(fileName);
  if (NULL != basePath)
    strLength = strlen(basePath) + strlen(fileName);
  char *fullPath = malloc(strLength+1);
  if (NULL != basePath)
  {
    LOG(EBICS_LOGLEVEL_DEBUG, "len(basePath): %u",strlen(basePath));
    LOG(EBICS_LOGLEVEL_DEBUG, "len(fileName): %u",strlen(fileName));
    strcpy(fullPath, basePath);
    strcat(fullPath, "/");
    strcat(fullPath, fileName);
  }
  else
  {
    strcpy(fullPath, fileName);
  }

  LOG (EBICS_LOGLEVEL_DEBUG, "Path: '%s'", fullPath);
  return fullPath;
}
