/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/

#define FINTS_NS_TYPES = "http://www.fints.org/spec/xmlschema/4.1/types"
#define FINTS_NS_MESSAGES = "http://www.fints.org/spec/xmlschema/4.1/messages"
#define FINTS_NS_TRANSACTIONS = ""
#define FINTS_NS_ADMINTRANSACTIONS = ""

struct FINTS_BankID {
    char *country_code;
      char *bank_code;
};


/**
 * Namespaces needed for message and item building
 */
struct FINTS_xml_Namespaces
{
  xmlNsPtr types;
  xmlNsPtr messages;
  xmlNsPtr transactions;
  xmlNsPtr admintransactions;
};

/**
 * Generate a BankID item
 */
xmlNodePtr
msg_gen_BankID(const struct FINTS_xml_Namespaces *ns, const struct FINTS_BankID *bankID);

xmlNodePtr
msg_gen_ReqMsgHeader(const struct FINTS_xml_Namespaces *ns,
                     xmlNodePtr BankID,
                     const char *UserRef,
                     const char *UserTextRef,
                     const char *BankRef,
                     const char *MsgNo);

xmlNodePtr
msg_gen_AnonymousIdentification(const struct FINTS_xml_Namespaces *ns,
                                xmlNodePtr BankID);

xmlNodePtr
msg_gen_PersonalizedIdentification(const struct FINTS_xml_Namespaces *ns,
                                   xmlNodePtr BankID,
                                   const char *CustID);

xmlNodePtr
msg_gen_ProcPreparation(const struct FINTS_xml_Namespaces *ns,
                        const char *BpdVersion,
                        const char *UpdVersion,
                        const char *SessionLang,
                        const char *ProductName,
                        const char *ProductVersion,
                        const char *ProductID);

xmlNodePtr
msg_gen_InitReq(const struct FINTS_xml_Namespaces *ns,
                xmlNodePtr Identification,
                xmlNodePtr ProcPreparation);
