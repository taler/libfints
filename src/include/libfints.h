/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/


#ifndef LIBFINTS_H
#define LIBFINTS_H

#define FINTS_LEN_COUNTRYCODE
#define FINTS_LEN_COUNTRYCODE_NUM
#define FINTS_LEN_BANKCODE
#define FINTS_LEN_ID
#define FINTS_LEN_BANKID
#define FINTS_LEN_

#define FINTS_LEN_PINTAN_CHAL // TODO: 99 or 2048 for cahl and/or response?
#define FINTS_LEN_PINTAN_RESP

#define FINTS_LEN_

/**
 * FINTS_IBAN: As defined by ECBS EBS204 (ISO 13616:2007)
 * Format (length in UTF8 chars):
 * countrycode[2] as defined in ISO 3166-1
 * checksum[2] as defined by ISO 7064
 * account_number[30] countryspecific
 */
struct FINTS_IBAN {
  char *country;
  char *checksum;
  char *account_number;
};

/**
 * FINTS_IBAN_to_string converts from FINTS_IBAN struct to char array.
 */
char *
FINTS_IBAN_to_string(const struct FINTS_IBAN *iban);

/**
 * FINTS_string_to_IBAN converts from char array to FINTS_IBAN struct.
 */
struct FINTS_IBAN
FINTS_string_to_IBAN(const char *iban);

/**
 * FINTS_BIC as defined by ISO 9362.
 * Format (length in UTF8 chars):
 * bankcode[4] arbitrarily selected by bank.
 * country[2] as defined in ISO 3166-1.
 * location[2] location of bank, may not start with "1" or "2".
 * branch[3] branch code, may not start with X except it is XXX.
 */
struct FINTS_BIC {
  char *bankcode;
  char *country;
  char *location;
  char *branch;
};

/**
 * FINTS_BIC_to_string converts from FINTS_BIC struct to char array.
 */
char *
FINTS_BIC_to_string(const struct FINTS_BIC *bic);

/**
 * FINTS_string_to_IBAN converts from char array to FINTS_IBAN struct.
 */
struct FINTS_BIC
FINTS_string_to_IBAN(const char *bic);

/**
 * FINTS_BankID [Messages:II.2] contains information to uniquely identify a bank.
 * Format (maximum length in UTF8 chars):
 * country-code[FINTS_LEN_COUNTRYCODE_NUM] as defined in ISO 3166-1 (numerical),
 * bank_code[FINTS_LEN_BANKCODE] national bank code
 */
struct FINTS_BankID {
  char *country_code;
  char *bank_code;
};

/**
 * Available account types.
 * Complete Account should be used, national or international account can be extracted if needed.
 */
enum FINTS_AccountType {
  FINTS_AT_NATIONAL_ACCOUNT,
  FINTS_AT_INTERNATIONAL_ACCOUNT,
  FINTS_AT_COMPLETE_ACCOUNT
};

/**
 * National account info [Messages:II.3.1].
 * Format (maximum length in utf8 chars):
 * acct_number[FINTS_LEN_ID],
 * subacct_identifier[FINTS_LEN_ID],
 */
struct FINTS_NationalAcctInfo {
  char *acct_number;
  char *subacct_chr;
  struct FINTS_BankID bank_id;
};

/**
 * International account info [Messages:II.3.2].
 */
struct FINTS_IntAcctInfo {
  struct FINTS_IBAN iban;
  struct FINTS_BIC bic;
};

/**
 * Complete account info [Messages:II.3.3].
 */
struct FINTS_FINTS_CompleteAcctInfo {
  struct FINTS_IntAcctInfo int_account;
  struct FINTS_NationalAcctInfo nat_account;
};

/**
 * General account.
 */
struct FINTS_Account {
  enum FINTS_AccountType type;
  union {
    struct FINTS_IntAcctInfo intl_acct;
    struct FINTS_NationalAcctInfo natl_acct;
    struct FINTS_CompleteAcctInfo comp_acct;
  } account;
};


/**
 * Monetary amount [Messages:II.1].
 */
struct FINTS_Amount {
  /** Sign of the Amount */
  uint8_t sign;
  /** Full units of currency */
  uint64_t full_units;
  /** Fractional Units of Currency in 1/10e14 */
  uint64_t fractional_units;
  /** ISO 4217 Code, 3 characters */
  char *currency;
};


/**
 * Authentication methods.
 *
 * Available authentication methods.
 */
enum FINTS_AuthMethod {
  AM_PIN = 1,
  AM_TAN = 2,
  AM_HBCI_RAH7 = 4,
  AM_HBCI_RAH9 = 8
};



/**
 * FINTS_ErrorCode for general return values.
 */
enum FINTS_ErrorCode
{
   FINTS_YES = 1,
   FINTS_NO = 0,
   FINTS_ERROR = -1,
  // ... more error codes? more error codes!
};


enum FINTS_Options
{
  FINTS_OP_NOOP = 0;
};


/**
 * FINTS_Bank, holds all data needed to communicate with the bank.
 */
struct FINTS_Bank
{
  struct FINTS_BankID;
  char *url;
};


/**
 * Create a new bank handle.
 */
struct FINTS_Bank *
FINTS_bank_open (const char *url,
                 enum FINTS_Options options,
                  ...);

/**
 * Close bank handle.
 */
void
FINTS_bank_close (struct FINTS_Bank *bank);


/**
* Return #FINTS_ERROR on error, #FINTS_NO on no timeout, #FINTS_YES if timeout was set
*/
enum FINTS_ErrorCode
FINTS_bank_get_fdset (struct FINTS_Bank *bank,
                       fd_set *rs,
                       fd_set *ws,
                       int *max_fd,
                       long long *timeout_ms);

/**
 * Perform FINTS_Operation.
 */
enum FINTS_ErrorCode
FINTS_bank_perform (struct FINTS_Bank *bank,
                     const fd_set *rs,
                     const fd_set *ws);

// Kunde
// TODO What else is needed for customer?
struct FINTS_Customer
{
  char cust_id[FINTS_LEN_ID];
};


// Benutzer
// TODO more info needed for user?
struct FINTS_User
{
  char user_id[FINTS_LEN_ID];
  //optional
  size_t legal_name_len;
  char *legal_name;
  size_t legal_address_len;
  char *legal_address;
};

/**
 * FINTS_PINTAN_Challenge contains information/text for the customer regarding the PIN/TAN request.
 */
struct FINTS_PINTAN_Challenge
{
  char *request; /** Maximum length in UTF8 chars of FINTS_LEN_PINTAN_CHALL */
};

/**
 * FINTS_PINTAN_ChallengeResponse contains the response of the user to a PIN/TAN Challenge.
 */
struct FINTS_PINTAN_ChallengeResponse
{
  char *response; /** Maximum length in UTF8 chars of FINTS_LEN_PINTAN_RESP */
};

/**
 * FINTS_ChallengeType specifies the type of challenge.
 */
enum FINTS_ChallengeType {
  FINTS_CT_NO_CHALLENGE,
  FINTS_CT_PIN,
  FINTS_CT_TAN,
  FINTS_CT_XML_SIGNATURE,
  FINTS_CT_SECODER_SIGNATURE
};

/**
 * FINTS_Challenge, contains the challenge type negotiated by bank and a matching
 * struct with the challenge data.
 */
struct FINTS_Challenge
{
  enum FINTS_ChallengeType challenge;
  union
  {
    struct FINTS_PINTAN_Challenge pintan;
    struct FINTS_XMLSIG_Challenge xmlsig;
  } data;
};

/**
 * FINTS_ResponseType specifies the type of the response.
 */
enum FINTS_ResponseType {
  FINTS_RT_NO_RESPONSE,
  FINTS_RT_PIN,
  FINTS_RT_TAN,
  FINTS_RT_XML_SIGNATURE,
  FINTS_RT_SECODER_SIGNATURE
};

/**
 * FINTS_Response, contains the response type and matching struct with the response.
 */
struct FINTS_Response
{
  enum FINTS_ResponseType response;
  union
  {
    struct FINTS_PINTAN_ChallengeResponse pintan;
    struct FINTS_XMLSIG_ChallengeResponse xmlsig;
  } data;
};

/**
 * Is called once the balance request has finished and a balance is available.
 */
void
(*FINTS_BalanceCallback) (void *cls,
                          const struct FINTS_Amount *amount,
                          const FINTS_AbsoluteTime timestamp);

/**
 * FINTS_AuthorizationRequest identifies the request.
 */
struct FINTS_AuthorizationRequest;

/**
 * Callback signaling the need for an authorization
 */
void
(*FINTS_AuthorizationCallback)(void *cls,
                               struct FINTS_AuthorizationRequest *req,
                               const struct FINTS_Challenge *challenge);

/**
 * Function to abort a authorization.
 */
void
FINTS_authorization_abort (struct FINTS_AuthorizationRequest *req);

/**
 * Function to respond to a authorization challenge
 */
void
FINTS_authorization_reply (struct FINTS_AuthorizationRequest *req,
                            const struct FINTS_Response *resp);

/**
 * FINTS_Oprtation identifies a operation.
 */
struct FINTS_Operation;

/**
 * Function to cancle a operation.
 */
void
FINTS_cancel (struct FINTS_Operation *op);

/**
 * Get the current balance of the account.
 */
struct FINTS_Operation *
FINTS_account_get_balance (const struct FINTS_Account *account,
                          const struct FINTS_User *user,
                          FINTS_BalanceCallback bc, void *bc_cls,
                          FINTS_AuthorizationCallback ac, void *ac_cls);


/**
 * Contains one transfer.
 */
struct FINTS_Transfer
{
  const struct FINTS_Account *source; /** Source account */
  const struct FINTS_Account *destination; /** Destination account */
  const struct FINTS_Amount *amount; /** Amount of transfer */
  struct FINTS_AbsoluteTime execution_time; /** Date of validation */
  char *purpose; /** Purpose of transaction */
  char *reference; /** Reference of transaction */
};


/**
 * Callback for transaction history elements
 */
void
(*FINTS_HistoryCallback) (void *cls,
                          const struct FINTS_Transfer *transfer);

/**
 * Function to get the account history.
 */
struct FINTS_Operation *
FINTS_account_get_history (const struct FINTS_Account *account,
                           const struct FINTS_User *user,
                           struct FINTS_AbsoluteTime start,
                           struct FINTS_AbsoluteTime end,
                           FINTS_HistoryCallback bc, void *bc_cls,
                           FINTS_AuthorizationCallback ac, void *ac_cls);

/**
 * Callback with prepared transaction.
 */
void
(*FINTS_PrepareTransactionCallback) (void *cls,
                                     const char *buf,
                                     size_t buf_size);

/**
 * Function to prepare a transaction.
 */
struct FINTS_Operation *
FINTS_account_prepare_transaction (const struct FINTS_Account *account,
                                   const struct FINTS_User *user,
                                   FINTS_PrepareTransactionCallback ptc, void *ptc_cls,
                                   FINTS_AuthorizationCallback ac, void *ac_cls);

/**
 * Callback for confirmation of transaction.
 *
 * @param cls closure
 * @param buf transaction data to persist
 * @param buf_size number of bytes in @a buf
 */
void
(*FINTS_ConfirmTransactionCallback) (void *cls,
                                     const char *buf,
                                     size_t buf_size);

struct FINTS_Operation *
FINTS_account_execute_transaction (const struct FINTS_Account *account,
                                   const struct FINTS_User *user,
                                   const char *buf, size_t buf_size,
                                   FINTS_AuthorizationCallback ac, void *ac_cls,
                                   FINTS_ConfirmTransactionCallback ctc, void *ctc_cls);


#endif
