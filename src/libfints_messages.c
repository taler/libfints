/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/


#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlschemastypes.h>

#include "include/libfints_messages.h"

#define LIBXML_SCHEMAS_ENABLED
#if defined(LIBXML_TREE_ENABLED) && defined(LIBXML_OUTPUT_ENABLED)





/**
 * FINTS_xml_gen_BankID
 * Creates a new xmlTree containing a BankID.
 * 
 * @param struct FINTS_BankID *bankID
 * @param struct FINTS_xml_Namespaces *ns
 * @returns xmlNodePtr
 */
xmlNodePtr
msg_gen_BankID(const struct FINTS_xml_Namespaces *ns, const struct FINTS_BankID *bankID)
{
  xmlNodePtr root_node = NULL;

  
  root_node = xmlNewNode(ns->messages, BAD_CAST "BankID");
  xmlNewTextChild(root_node, ns->types, BAD_CAST "CountryCode", BAD_CAST bankID->country_code);
  if (NULL != bankID->bank_code)
  {
    xmlNewTextChild(root_node, ns->types, BAD_CAST "BankCode", BAD_CAST bankID->bank_code);
  }


  return root_node;
}


xmlNodePtr
msg_gen_ReqMsgHeader(const struct FINTS_xml_Namespaces *ns,
                     xmlNodePtr BankID,
                     const char *UserRef,
                     const char *UserTextRef,
                     const char *BankRef,
                     const char *MsgNo)
{
  xmlNodePtr root_node = NULL;

  
  root_node = xmlNewNode(ns->messages, BAD_CAST "ReqMsgHeader");
  xmlAddChild(root_node, BankID);
  xmlNewChild(root_node, NULL, BAD_CAST "UserRef", BAD_CAST UserRef);
  xmlNewChild(root_node, NULL, BAD_CAST "UserTextRef", BAD_CAST UserTextRef);
  xmlNewChild(root_node, NULL, BAD_CAST "MsgNo", BAD_CAST MsgNo);

  return root_node;
};


xmlNodePtr
msg_gen_AnonymousIdentification(const struct FINTS_xml_Namespaces *ns,
                                xmlNodePtr BankID)
{
  xmlNodePtr root_node = xmlNewNode(ns->messages, BAD_CAST "AnonymousIdentification");
  xmlAddChild(root_node, BankID);
  return root_node;
}

xmlNodePtr
msg_gen_PersonalizedIdentification(const struct FINTS_xml_Namespaces *ns,
                                   xmlNodePtr BankID,
                                   const char *CustID)
{
  xmlNodePtr root_node = xmlNewNode(ns->messages, BAD_CAST "PersonalizedIdentification");
  xmlAddChild(root_node, BankID);
  xmlNewChild(root_node, NULL, BAD_CAST "CustID", BAD_CAST CustID);
  return root_node;
}

xmlNodePtr
msg_gen_ProcPreparation(const struct FINTS_xml_Namespaces *ns,
                        const char *BpdVersion,
                        const char *UpdVersion,
                        const char *SessionLang,
                        const char *ProductName,
                        const char *ProductVersion,
                        const char *ProductID)
{
  xmlNodePtr root_node = xmlNewNode(ns->messages, BAD_CAST "ProcPreparation");
  if (BpdVersion)
    xmlNewChild(root_node, NULL, BAD_CAST "BpdVersion", BAD_CAST BpdVersion);
  if (UpdVersion)
    xmlNewChild(root_node, NULL, BAD_CAST "UpdVersion", BAD_CAST UpdVersion);
  if (SessionLang)
    xmlNewChild(root_node, NULL, BAD_CAST "SessionLang", BAD_CAST SessionLang);

  xmlNewChild(root_node, NULL, BAD_CAST "ProductName", BAD_CAST ProductName);
  xmlNewChild(root_node, NULL, BAD_CAST "ProductVersion", BAD_CAST ProductVersion);
  if (ProductID)
    xmlNewChild(root_node, NULL, BAD_CAST "ProductID", BAD_CAST ProductID);
  return root_node;
}

xmlNodePtr
msg_gen_InitReq(const struct FINTS_xml_Namespaces *ns,
                const xmlNodePtr Identification,
                const xmlNodePtr ProcPreparation)
{
  xmlNodePtr root_node = xmlNewNode(ns->messages, BAD_CAST "InitReq");
  if (Identification)
    xmlAddChild(root_node, Identification);
  if (ProcPreparation)
    xmlAddChild(root_node, ProcPreparation);
  return root_node;
}
/*
*/
#endif
