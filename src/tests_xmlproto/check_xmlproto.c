/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/

#include <stdlib.h>
#include <assert.h>
#include <check.h>
#include "../util.h"
#include "../xmlproto.h"

#define LOG(level,...) EBICS_util_log_from (__LINE__,__FILE__,__func__,level, "check_xmlproto",__VA_ARGS__)

int
util_generate_referencefile(const char *path, xmlDocPtr document)
{
#ifdef GENERATE_REFFILES
  /* TODO: Write out generated file */
  LOG (EBICS_LOGLEVEL_DEBUG, "Writing to: '%s'", path);
  FILE *f = fopen(path, "w");
  int ret = xmlDocFormatDump(f, document, 1);
  fclose(f);
  return ret;
#endif
  return -1;
}

int
util_ck_compare_documents(xmlDocPtr doc1, xmlDocPtr doc2)
{
  xmlChar *doc1Buf;
  xmlChar *doc2Buf;
  int doc1Size, doc2Size;
  xmlDocDumpFormatMemoryEnc(doc1, &doc1Buf, &doc1Size, "UTF-8", 1);
  xmlDocDumpFormatMemoryEnc(doc2, &doc2Buf, &doc2Size, "UTF-8", 1);

  if (0 != strcmp(doc1Buf, doc2Buf))
    //TODO!
    return 1;

  xmlFree(doc1Buf);
  xmlFree(doc2Buf);
}

int
test_operation_set_string()
{
  int retv;
  char *testXPath = "//set_string";

  /* Init the xml library */
  xmlInitParser();

  /* Build the paths to our xml input files */
  char *srcDir = getenv("srcdir");
  char *fullInPath = util_generate_path(srcDir, "xmlproto_op_input.xml");
  char *fullOutPath = util_generate_path(srcDir, "xmlproto_op_set_string_output.xml");

  /* Load the input XML file */
  struct EBICS_genex_document *testDoc = util_genex_document_from_file(fullInPath);
  struct EBICS_MSG_Spec test[] = 
  {
    EBICS_MSG_op_set_string(testXPath, "TESTSTRING"),
    EBICS_MSG_op_end()
  };
  EBICS_MSG_parse_spec(test, testDoc);
  
  /* Test if the value was set as expected */
  LOG(EBICS_LOGLEVEL_DEBUG, "testXPath: %p, ctxt: %p", testXPath, testDoc->xpath);
  xmlChar *result = util_test_xpath_value(testXPath, testDoc->xpath);
  if (0 != strcmp("TESTSTRING",result));
    // TODO!
    return 1;
  xmlFree(result);


  /* Saves generated document to disk if GENERATE_REFFILES is defined */
  retv = util_generate_referencefile(fullOutPath, testDoc->document);

  /* Load reference file and compare it to generated document */
  struct EBICS_genex_document *expectedDoc = util_genex_document_from_file(fullOutPath);

  retv = util_ck_compare_documents(testDoc->document, expectedDoc->document);
  util_genex_free_document(expectedDoc);
  util_genex_free_document(testDoc);
  free(fullInPath);
  free(fullOutPath);

  /* Cleanup the xml library */
  xmlCleanupParser();

  return 0;
}

int
test_operation_set_int()
{
  int retv;
  char *testXPath = "//set_int";

  /* Init the xml library */
  xmlInitParser();

  /* Build the paths to our xml input files */
  char *fullInPath = util_generate_path(getenv("srcdir"), "xmlproto_op_input.xml");
  char *fullOutPath = util_generate_path(getenv("srcdir"), "xmlproto_op_set_int_output.xml");

  /* Load the input XML file */
  struct EBICS_genex_document *testDoc = util_genex_document_from_file(fullInPath);
  struct EBICS_MSG_Spec test[] = 
  {
    EBICS_MSG_op_set_int(testXPath, -1234567890),
    EBICS_MSG_op_end()
  };
  EBICS_MSG_parse_spec(test, testDoc);

  /* Saves generated document to disk if GENERATE_REFFILES is defined */
  retv = util_generate_referencefile(fullOutPath, testDoc->document);
  
  /* Test if the value was set as expected */
  xmlChar *result = util_test_xpath_value(testXPath, testDoc->xpath);
  assert(0 == strcmp("-1234567890",result));
  xmlFree(result);

  /* Load reference file and compare it to generated document */
  struct EBICS_genex_document *expectedDoc = util_genex_document_from_file(fullOutPath);

  retv = util_ck_compare_documents(testDoc->document, expectedDoc->document);
  util_genex_free_document(expectedDoc);
  util_genex_free_document(testDoc);
  free(fullInPath);
  free(fullOutPath);

  /* Cleanup the xml library */
  xmlCleanupParser();

  return 0;
}

int
test_operation_set_uint()
{
  int retv;
  char *testXPath = "//set_uint";

  /* Init the xml library */
  xmlInitParser();

  /* Build the paths to our xml input files */
  char *fullInPath = util_generate_path(getenv("srcdir"), "xmlproto_op_input.xml");
  char *fullOutPath = util_generate_path(getenv("srcdir"), "xmlproto_op_set_uint_output.xml");

  /* Load the input XML file */
  struct EBICS_genex_document *testDoc = util_genex_document_from_file(fullInPath);
  struct EBICS_MSG_Spec test[] = 
  {
    EBICS_MSG_op_set_uint(testXPath, 1234567890),
    EBICS_MSG_op_end()
  };
  EBICS_MSG_parse_spec(test, testDoc);
  
  /* Test if the value was set as expected */
  xmlChar *result = util_test_xpath_value(testXPath, testDoc->xpath);
  assert(0 == strcmp("1234567890",result));
  xmlFree(result);


  /* Saves generated document to disk if GENERATE_REFFILES is defined */
  retv = util_generate_referencefile(fullOutPath, testDoc->document);

  /* Load reference file and compare it to generated document */
  struct EBICS_genex_document *expectedDoc = util_genex_document_from_file(fullOutPath);

  retv = util_ck_compare_documents(testDoc->document, expectedDoc->document);
  util_genex_free_document(expectedDoc);
  util_genex_free_document(testDoc);
  free(fullInPath);
  free(fullOutPath);

  /* Cleanup the xml library */
  xmlCleanupParser();
  return 0;
}


int main(void)
{

  int testsFailed = 0;

  testsFailed += test_operation_set_string();
  testsFailed += test_operation_set_uint();
  testsFailed += test_operation_set_int();
  
  LOG(EBICS_LOGLEVEL_DEBUG, "Failed: %d", testsFailed);
  return (testsFailed == 0) ? 0 : 1;
}
