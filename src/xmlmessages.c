/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/

#include "xmlmessages.h"
#include "libebics.h"
#include "gnunet/platform.h"
#include "gnunet/gnunet_util_lib.h"

/**
 * Size of the binary nonce
 */
#define EBICS_NONCE_BINARY_SIZE 16

/**
 * Size of the binhex formatted nonce plus 0-byte
 */
#define EBICS_NONCE_STRING_SIZE 33

#define LOG(level,...) \
  EBICS_util_log_from (__LINE__,  \
                       __FILE__, \
                       __func__, \
                       level, \
                       "xmlmessages", \
                       __VA_ARGS__)

extern struct EBICS_Key keyList[];

/**
 * Needed to get payload's templates
 */
extern struct EBICS_genex_document *
get_genex_instance (unsigned int type_index);


/**
 * Dump a EBICS_genex_document to LOG_DEBUG.
 *
 */
static void
dump_message (struct EBICS_genex_document *document)
{ 
  int buffersize;
  xmlChar* xmlbuf;

  xmlDocDumpFormatMemoryEnc (document->document,
                             &xmlbuf,
                             &buffersize,
                             "UTF-8", 1);
  LOG (EBICS_LOGLEVEL_DEBUG,
       "Dumping Document: %s\n%s\n",
       document->name,
       xmlbuf);

  xmlFree (xmlbuf);
}

/**
 * Return a Nonce
 */
static char*
tools_get_nonce(char *nonce)
{
  uint8_t index;
  unsigned char data[EBICS_NONCE_BINARY_SIZE];
  gcry_create_nonce (data, EBICS_NONCE_BINARY_SIZE);

  for (index = 0;
       index < EBICS_NONCE_BINARY_SIZE;
       index ++ )
  {
    LOG (EBICS_LOGLEVEL_DEBUG,
         "data[%u]: %02X\n",
         index,
         data[index]);
    snprintf (&nonce[index*2],
              3,
              "%02X",
              data[index]);
  }

  LOG (EBICS_LOGLEVEL_DEBUG,
       "Nonce: %s",
       nonce);
  return nonce;
}

/**
 * Return a timestamp
 */
static char *
tools_get_timestamp(char *date)
{
  time_t rawtime = time(NULL);
  struct tm *tmtime = gmtime (&rawtime);
  strftime (date, DATE_STR_SIZE, "%FT%TZ", tmtime);
  return date;
}


/**
 * Todo
 */
static int
util_extract_public_RSAKeyValue (struct EBICS_Key *key,
                                 char **mData,
                                 char **eData)
{

  int retv;
  size_t eSize;
  size_t mSize;
  gnutls_datum_t mod;
  gnutls_datum_t exp;


  GNUNET_assert
    (0 != (EBICS_KEY_RSA_PUBLIC & key->type));
  LOG (EBICS_LOGLEVEL_DEBUG,
       "Extracting modulus and exponent from key");

  retv = gnutls_pubkey_export_rsa_raw (key->publickey,
                                       &mod,
                                       &exp);
  if (GNUTLS_E_SUCCESS != retv)
  {
    LOG (EBICS_LOGLEVEL_ERROR,
         "Barf! %s",
         gnutls_strerror (retv));
    return EBICS_ERROR;
  }
  eSize = EBICS_UTIL_base64_encode ((char*) exp.data,
                                    exp.size,
                                    eData);
  mSize = EBICS_UTIL_base64_encode ((char*) mod.data,
                                    mod.size,
                                    mData);
  gnutls_free (mod.data);
  gnutls_free (exp.data);

  *eData = realloc (*eData, eSize+1);
  *mData = realloc (*mData, mSize+1);
  GNUNET_assert (eData != NULL);
  GNUNET_assert (mData != NULL);

  (*eData)[eSize] = 0;
  (*mData)[mSize] = 0;

  LOG (EBICS_LOGLEVEL_DEBUG,
       "EXPONENT: Size: %u, base64: %s",
       eSize,
       *eData);

  LOG (EBICS_LOGLEVEL_DEBUG,
       "MODULUS: Size: %u, base64: %s",
       mSize,
       *mData);

  return EBICS_SUCCESS;
} 


/**
 * Fill header with always-needed values.
 *
 * @param cls closure, tipically contains
 *        data to put in the document.
 * @param document the base document to fill.
 */
void
EBICS_build_header_generic (void *cls,
                            struct EBICS_genex_document *document)
{
  char *sigAttribute[] = {"DZHNN", "OZHNN"};
  struct EBICS_ARGS_build_header *data = (
    struct EBICS_ARGS_build_header*) cls;

  struct EBICS_MSG_Spec header[] = {
    EBICS_MSG_op_unique_choice ("//ebics:static"),
    EBICS_MSG_op_set_string ("//ebics:static//ebics:HostID",
                           data->hostID),
    EBICS_MSG_op_set_string ("//ebics:static//ebics:PartnerID",
                           data->partnerID),
    EBICS_MSG_op_set_string ("//ebics:static//ebics:UserID",
                           data->userID),
    EBICS_MSG_op_set_string ("//ebics:static//ebics:Product",
                             data->productName),
    EBICS_MSG_op_set_string ("//ebics:static//ebics:SecurityMedium",
                           "0000"),
    EBICS_MSG_op_set_attribute ("//ebics:static//ebics:Product/@Language",
                              data->languageCode),
    EBICS_MSG_op_unique_choice ("//ebics:OrderDetails"),
    EBICS_MSG_op_set_string ("//ebics:OrderDetails/ebics:OrderAttribute",
                           sigAttribute[data->requestBankSignature]),
    EBICS_MSG_op_end ()
  };
  EBICS_MSG_parse_spec (header,
                      document);
}


/**
 * 
 */
void
EBICS_build_header_ebicsRequest (void *cls,
                                 struct EBICS_genex_document *document)
{
  char nonce[EBICS_NONCE_STRING_SIZE];
  char date[DATE_STR_SIZE];
  struct EBICS_MSG_Spec header[] = {

    EBICS_MSG_op_subcommand (EBICS_build_header_generic,
                             cls),

    EBICS_MSG_op_set_string ("//ebics:static//ebics:Nonce",
                             tools_get_nonce (nonce)),

    EBICS_MSG_op_set_string ("//ebics:static//ebics:Timestamp",
                             tools_get_timestamp (date)),

    /* TODO: test if header should be initializing or not */
    EBICS_MSG_op_set_string ("ebics:TransactionPhase",
                             "Initialisation"),
    EBICS_MSG_op_end ()
  };

  EBICS_MSG_parse_spec (header,
                      document);
}


/**
 * Among the most important things, this function
 * causes the HostID value to be set in the document.
 */
void
EBICS_build_header_ebicsUnsecuredRequest (void *cls,
                                          struct EBICS_genex_document *document)
{
  struct EBICS_MSG_Spec header[] = {
    EBICS_MSG_op_subcommand (EBICS_build_header_generic,
                             cls),
    EBICS_MSG_op_end ()
  };

  EBICS_MSG_parse_spec (header,
                      document);
}

void
EBICS_build_header_ebicsNoPubKeyDigestsRequest (void *cls,
                                                struct EBICS_genex_document *document)
{
  char nonce[EBICS_NONCE_STRING_SIZE];
  char date[DATE_STR_SIZE];
  struct EBICS_MSG_Spec header[] = {
    EBICS_MSG_op_subcommand (EBICS_build_header_generic,
                             cls),
    EBICS_MSG_op_set_string ("//ebics:static//ebics:Nonce",
                             tools_get_nonce (nonce)),
    EBICS_MSG_op_set_string ("//ebics:static//ebics:Timestamp",
                             tools_get_timestamp (date)),
    EBICS_MSG_op_end ()
  };

  EBICS_MSG_parse_spec (header, document);
}

void
EBICS_build_content_ini (void *cls,
                         struct EBICS_genex_document *document)
{
  int retv;
  char *modulus;
  char *exponent;
  char date[DATE_STR_SIZE];
  struct EBICS_ARGS_build_content_ini *data;
  struct EBICS_genex_document *payload;

  xmlChar *iniContent;
  char *base64content;
  size_t zLen, b64len;
  int xLen;
  char *b64data;

  data = (struct EBICS_ARGS_build_content_ini*) cls;

  util_extract_public_RSAKeyValue
    (&keyList[EBICS_USER_ES_KEY],
     &exponent,
     &modulus);

  /* Fills the SignaturePubKeyOrderData sub-tree,
   * that will be appended to the OrderData node.  */

  struct EBICS_MSG_Spec payload_spec[] = {
    EBICS_MSG_op_unique_choice ("//esig:SignatureVersion"),
    EBICS_MSG_op_del_node ("//ds:X509Data"),
    EBICS_MSG_op_set_string ("//esig:SignatureVersion",
                             "A005"),
    EBICS_MSG_op_set_string ("//esig:PubKeyValue//ds:Modulus",
                             modulus),
    EBICS_MSG_op_set_string ("//esig:TimeStamp",
                             tools_get_timestamp (date)),
    EBICS_MSG_op_set_string ("//esig:PartnerID",
                             data->partnerID),
    EBICS_MSG_op_set_string ("//esig:UserID",
                             data->userID),
    EBICS_MSG_op_del_node ("//schema:ANY"),
    EBICS_MSG_op_set_string ("//esig:PubKeyValue//ds:Exponent",
                             exponent),
    EBICS_MSG_op_end ()
  };

  payload = get_genex_instance (EBICS_INI_PAYLOAD_TEMPLATE);
  EBICS_MSG_parse_spec (payload_spec,
                        payload);
  GNUNET_free (modulus);
  GNUNET_free (exponent);
  
  /* insert base64'ed and zlib'ed data */
  LOG (EBICS_LOGLEVEL_DEBUG,
       "DUMPING CONTENT!");
  dump_message (document);
  xmlDocDumpMemoryEnc (payload->document,
                       &iniContent,
                       &xLen,
                       "utf-8");

  zLen = compressBound ((size_t) xLen);
  char *zData = malloc (zLen);
  retv = compress ((unsigned char *) zData,
                   &zLen,
                   iniContent,
                   xLen);

  LOG (EBICS_LOGLEVEL_INFO,
       "Size: %lu, FinalSize: %lu",
       (size_t) xLen,
       zLen);

  b64len = EBICS_UTIL_base64_encode ((char *) zData,
                                     zLen,
                                     &base64content);

  b64data = realloc (base64content,
                     b64len + 1);

  /* 0-terminate the b64 segment */
  b64data[b64len] = 0;
  base64content = b64data;

  LOG (EBICS_LOGLEVEL_INFO,
       "Compressed and base64ed. Len: %lu, Content:\n%s",
       b64len,
       base64content);

  struct EBICS_MSG_Spec body[] = {
    EBICS_MSG_op_set_string
      ("//ebics:OrderDetails//ebics:OrderType",
       "INI"),
    EBICS_MSG_op_set_string
      ("//ebics:body//ebics:DataTransfer/ebics:OrderData",
       base64content),

    EBICS_MSG_op_end()
  };

  EBICS_MSG_parse_spec (body,
                        document);
  xmlFree (iniContent);
  GNUNET_free (payload);
  GNUNET_free (base64content);
  GNUNET_free (zData);
}


/**
 * Make the payload for HIA messages.
 *
 * @param cls closure, contains the values that
 *        make the message.
 * @param document the final document.
 */
void
EBICS_build_content_hia (void *cls,
                         struct EBICS_genex_document *document)
{

  int retv;
  char *encMod;
  char *encExp;
  char *sigMod;
  char *sigExp;
  char date[DATE_STR_SIZE];
  struct EBICS_genex_document *payload;

  struct EBICS_ARGS_build_content_hia *data = (
    struct EBICS_ARGS_build_content_hia*) cls;

  util_extract_public_RSAKeyValue
    (&keyList[EBICS_USER_ENC_KEY],
     &encExp,
     &encMod);

  util_extract_public_RSAKeyValue
    (&keyList[EBICS_USER_SIG_KEY],
     &sigExp,
     &sigMod);

  struct EBICS_MSG_Spec payload_spec[] = {
    EBICS_MSG_op_unique_choice
      ("//ebics:AuthenticationPubKeyInfo"),
    EBICS_MSG_op_del_node ("//ds:X509Data"),
    EBICS_MSG_op_set_string
      ("//ebics:AuthenticationVersion",
       "X002"),
    EBICS_MSG_op_set_string
      ("//ebics:AuthenticationPubKeyInfo//ds:Modulus",
       sigMod),
    EBICS_MSG_op_set_string
      ("//ebics:AuthenticationPubKeyInfo//ds:Exponent",
       sigExp),
    EBICS_MSG_op_set_string
      ("//ebics:AuthenticationPubKeyInfo//ebics:TimeStamp",
       tools_get_timestamp (date)),
    EBICS_MSG_op_set_string
      ("//ebics:EncryptionVersion",
       "E002"),
    EBICS_MSG_op_set_string
      ("//ebics:EncryptionPubKeyInfo//ds:Modulus",
       encMod),
    EBICS_MSG_op_set_string
      ("//ebics:EncryptionPubKeyInfo//ds:Exponent",
       encExp),
    EBICS_MSG_op_set_string
      ("//ebics:EncryptionPubKeyInfo//ebics:TimeStamp",
       tools_get_timestamp (date)),
    EBICS_MSG_op_set_string
      ("//ebics:PartnerID",
       data->partnerID),
    EBICS_MSG_op_set_string
      ("//ebics:UserID",
       data->userID),
    EBICS_MSG_op_del_node ("//schema:ANY"),
    EBICS_MSG_op_end ()
  };

  payload = get_genex_instance (EBICS_HIA_PAYLOAD_TEMPLATE);
  EBICS_MSG_parse_spec (payload_spec,
                        payload);

  LOG (EBICS_LOGLEVEL_DEBUG,
       "Leaving critical");

  free (encMod);
  free (encExp);
  free (sigMod);
  free (sigExp);
  
  /* insert base64'ed and zlib'ed data */
  xmlChar *iniContent;
  char *base64Content;
  size_t zLen, b64Len;
  int xLen;

  LOG (EBICS_LOGLEVEL_DEBUG,
       "DUMPING CONTENT!\n");
  dump_message (payload);
  xmlDocDumpMemoryEnc (payload->document,
                       &iniContent,
                       &xLen,
                       "utf-8");

  zLen = compressBound ((size_t) xLen);
  char *zData = malloc (zLen);
  retv = compress ((unsigned char*) zData,
                   &zLen,
                   iniContent,
                   xLen);

  LOG (EBICS_LOGLEVEL_INFO,
       "Size: %lu, FinalSize: %lu\n",
       (size_t) xLen,
       zLen);

  b64Len = EBICS_UTIL_base64_encode ((char*)zData,
                                     zLen,
                                     &base64Content);

  char *b64Data = realloc (base64Content,
                           b64Len+1);
  b64Data[b64Len] = 0;
  base64Content = b64Data;

  LOG (EBICS_LOGLEVEL_INFO,
       "Compressed and base64ed. len: %lu, Content:\n%s\n",
       b64Len,
       base64Content);

  struct EBICS_MSG_Spec body[] = {
    EBICS_MSG_op_set_string
      ("//ebics:OrderDetails/ebics:OrderType",
       "HIA"),
    EBICS_MSG_op_set_string
      ("//ebics:body//ebics:DataTransfer/ebics:OrderData",
       base64Content),

    EBICS_MSG_op_end ()
  };
  EBICS_MSG_parse_spec (body,
                        document);
  xmlFree (iniContent);
  free (base64Content);
  free (zData);
  free (payload);
}

void
EBICS_build_content_camt053 (void *cls, struct EBICS_genex_document *document)
{
  struct EBICS_ARGS_build_content_camt053 *data = (struct EBICS_ARGS_build_content_camt053*)cls;
  struct EBICS_MSG_Spec request[] = {
    EBICS_MSG_op_unique_choice("//ebics:OrderDetails"),
    EBICS_MSG_op_set_string("//ebics:OrderDetails/ebics:OrderType", "C53"),
    EBICS_MSG_op_unique_choice("//ebics:StandardOrderParams"),
    EBICS_MSG_op_set_date("//ebics:StandardOrderParams//ebics:Start", data->startdate), 
    EBICS_MSG_op_set_date("//ebics:StandardOrderParams//ebics:End", data->enddate), 
    EBICS_MSG_op_del_node("//ebics:body/ds:X509Data"),
    EBICS_MSG_op_select_choice("//ebics:body/schema:CHOICES", 0),
    EBICS_MSG_op_end()
  };
  EBICS_MSG_parse_spec(request, document);
}

void
EBICS_build_bankPubKeyDigest (void *cls, struct EBICS_genex_document *document)
{
  struct EBICS_ARGS_build_auth *data = (struct EBICS_ARGS_build_auth*)cls;

  struct EBICS_MSG_Spec auth[] = {
    EBICS_MSG_op_unique_choice("//ebics:BankPubKeyDigests"),

    /* Set bank authentication key */
    EBICS_MSG_op_set_attribute("//ebics:BankPubKeyDigests/ebics:Authentication/@Version",
                               "X002"),
    EBICS_MSG_op_set_attribute("//ebics:BankPubKeyDigests/ebics:Authentication/@Algorithm","F1"),
                              // hash_mapping[data->bankAuthentication.publickey.algorithm]),
    EBICS_MSG_op_set_string("//ebics:BankPubKeyDigests/ebics:Authentication","F2"),
                            //(char*)data->bankAuthentication.publickey.data),

    /* Set bank encryption key */
    EBICS_MSG_op_set_attribute("//ebics:BankPubKeyDigests/ebics:Encryption/@Version",
                               "E002"),
    EBICS_MSG_op_set_attribute("//ebics:BankPubKeyDigests/ebics:Encryption/@Algorithm","F3"),
                               //hash_mapping[data->bankEncryption.publickey.algorithm]),
    EBICS_MSG_op_set_string("//ebics:BankPubKeyDigests/ebics:Encryption","F4"),
                            //(char*)data->bankEncryption.publickey.data),
    EBICS_MSG_op_end()
  };
  EBICS_MSG_parse_spec(auth, document);
}

void
EBICS_build_auth_signature (void *cls,
                            struct EBICS_genex_document *document)
{
  char *hash_mapping[2] = {"http://www.w3.org/2001/04/xmlenc#sha256",
                           "http://www.w3.org/2001/04/xmlenc#sha512" };
  struct EBICS_ARGS_build_auth *data = (struct EBICS_ARGS_build_auth*) cls;

  struct EBICS_MSG_Spec auth[] = {
    /* Prepare Signature Part */
    EBICS_MSG_op_del_attribute ("//ebics:AuthSignature/@Id"),
    EBICS_MSG_op_del_attribute ("//ebics:AuthSignature/ds:SignedInfo/@Id"),
    EBICS_MSG_op_set_attribute ("//ebics:AuthSignature/ds:SignedInfo/ds:CanonicalizationMethod/@Algorithm",
                                "http://www.w3.org/TR/2001/REC-xml-c14n-20010315"),
    EBICS_MSG_op_set_attribute ("//ebics:AuthSignature/ds:SignedInfo/ds:SignatureMethod/@Algorithm",
                                "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"),
    EBICS_MSG_op_set_attribute ("//ebics:AuthSignature/ds:SignedInfo/ds:Reference/@URI",
                                "#xpointer(//*[@authenticate='true'])"),
    EBICS_MSG_op_del_attribute ("//ebics:AuthSignature/ds:SignedInfo/ds:Reference/@Id"),
    EBICS_MSG_op_del_attribute ("//ebics:AuthSignature/ds:SignedInfo/ds:Reference/@Type"),
    EBICS_MSG_op_set_attribute ("//ebics:AuthSignature//ds:Transform/@Algorithm",
                                "http://www.w3.org/TR/2001/REC-xml-c14n-20010315"),
    EBICS_MSG_op_set_attribute ("//ebics:AuthSignature//ds:DigestMethod/@Algorithm",
                                "http://www.w3.org/2001/04/xmlenc#sha256"),
    EBICS_MSG_op_set_string ("//ebics:AuthSignature//ds:DigestValue",
                             ""),
    EBICS_MSG_op_del_attribute ("//ebics:AuthSignature//ds:KeyInfo/@Id"),
    EBICS_MSG_op_unique_choice ("//ebics:AuthSignature//ds:KeyName"),
    EBICS_MSG_op_del_node ("//ebics:AuthSignature//ds:KeyName"),
    EBICS_MSG_op_del_attribute ("//ebics:AuthSignature/ds:SignatureValue/@Id"),
    EBICS_MSG_op_set_string ("//ebics:AuthSignature//ds:SignatureValue",
                             ""),
    EBICS_MSG_op_del_node ("//ebics:AuthSignature//ds:Object"),

    EBICS_MSG_op_end ()
  };
  EBICS_MSG_parse_spec (auth, document);
  LOG (EBICS_LOGLEVEL_DEBUG,
       "Message building finished, now signign");

  EBICS_sign_message (data->bankAuthentication,
                      data->bankEncryption,
                      data->userAuthentication,
                      document);
}

/**
 * Sign a EBICS message.
 *
 * @param bankAuthentication fixme UNUSED.
 * @param bankEncryption fixme UNUSED.
 * @param userAuthentication has the sign key.
 * @param document the document to equip with signature.
 */
void
EBICS_sign_message (const struct EBICS_Key *bankAuthentication, 
                    const struct EBICS_Key *bankEncryption, 
                    const struct EBICS_Key *userAuthentication, 
                    struct EBICS_genex_document *document)
{

  int res;
  int retv;
  xmlNodePtr node;
  xmlSecDSigCtxPtr dsigCtx;
  gnutls_datum_t out;

  /* Rename ebics:AuthSignature so 
   * xmlsec will not cry and break ... */
  {
    xmlXPathObjectPtr xpathObjPtr;
    xmlNodeSetPtr nodeset;
    xmlNodePtr node;
    xmlNsPtr ns;

    xpathObjPtr = xmlXPathEvalExpression
      (BAD_CAST "//ebics:AuthSignature", 
       document->xpath);

    GNUNET_assert (NULL != xpathObjPtr);
    GNUNET_assert (NULL != xpathObjPtr->nodesetval);

    nodeset = xpathObjPtr->nodesetval;
    node = nodeset->nodeTab[0];
    xmlXPathFreeObject (xpathObjPtr);
    ns = xmlSearchNsByHref (document->document,
                            node,
                            BAD_CAST "http://www.w3.org/2000/09/xmldsig#");
    xmlNodeSetName (node,
                    BAD_CAST "Signature");
    node->ns = ns;
  }
  
  node = NULL;
  dsigCtx = NULL;
  res = -1;

  node = xmlSecFindNode (xmlDocGetRootElement (document->document),
                         xmlSecNodeSignature,
                         BAD_CAST "http://www.w3.org/2000/09/xmldsig#");
  if (node == NULL)
  {

    LOG (EBICS_LOGLEVEL_ERROR, "Error: start node not found.");
    GNUNET_assert(0);      
  }

  dsigCtx = xmlSecDSigCtxCreate (NULL);
  xmlSecDSigCtxInitialize (dsigCtx, NULL);

  if(dsigCtx == NULL)
  {
    LOG (EBICS_LOGLEVEL_ERROR,
         "Error: failed to create signature context.");
    GNUNET_assert (0);
  }

  retv = gnutls_x509_privkey_export2 (userAuthentication->privatekey,
                                      GNUTLS_X509_FMT_DER,
                                      &out);
  if (GNUTLS_E_SUCCESS != retv)
  {
    LOG (EBICS_LOGLEVEL_ERROR,
         "Could not export key for reimport with xmlsec: %s",
         gnutls_strerror (retv));
  }

  dsigCtx->signKey = xmlSecCryptoAppKeyLoadMemory (out.data,
                                                   out.size,
                                                   xmlSecKeyDataFormatDer,
                                                   NULL,
                                                   NULL,
                                                   NULL);
  gnutls_free (out.data);

  if (dsigCtx->signKey == NULL)
  {
    LOG (EBICS_LOGLEVEL_ERROR,
         "Error: failed to load private pem key from buffer.");
    GNUNET_assert (0);
  }

  if (xmlSecKeySetName (dsigCtx->signKey,
                        BAD_CAST "userAuthKey") < 0)
  {
    LOG (EBICS_LOGLEVEL_FATAL,
         "Error: failed to set key name for key.");
    GNUNET_assert (0);
  }

  if (0 > xmlSecDSigCtxSign (dsigCtx,
                             node))
  {
    LOG (EBICS_LOGLEVEL_FATAL,"Error: signature failed.");
    // xmlSecDSigCtxDebugXmlDump (dsigCtx, stdout);
    GNUNET_assert (0);
  }

  LOG (EBICS_LOGLEVEL_DEBUG,
       "Error is below me!");

  if(dsigCtx != NULL) {
    /**
     * TODO: Calling Finalize generates some
     * assert errors inside xmlsec..Disabled for now.
     *
     * xmlSecDSigCtxFinalize(dsigCtx);
     **/
    xmlSecDSigCtxDestroy(dsigCtx);
  }

  LOG (EBICS_LOGLEVEL_DEBUG,
       "Error is Above me!");
  /**
   * Rename ds:Signature so the
   * message validates with ebics schema
   **/
  {
    xmlXPathObjectPtr xpathObjPtr;
    xmlNodeSetPtr nodeset;
    xmlNodePtr node;
    xmlNsPtr ns;

    xpathObjPtr = xmlXPathEvalExpression
      (BAD_CAST "//ds:Signature",
       document->xpath);

    GNUNET_assert (NULL != xpathObjPtr);
    GNUNET_assert (NULL != xpathObjPtr->nodesetval);
    nodeset = xpathObjPtr->nodesetval;
    node = nodeset->nodeTab[0];
    xmlXPathFreeObject (xpathObjPtr);
    ns = xmlSearchNsByHref (document->document,
                            node,
                            BAD_CAST "urn:org:ebics:H004");
    xmlNodeSetName (node,
                    BAD_CAST "AuthSignature");
    node->ns = ns;
  }
}
