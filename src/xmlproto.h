/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/

#ifndef EBICS_XML_PROTO_H
#define EBICS_XML_PROTO_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include <libxml/globals.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/xmlschemastypes.h>
#include <libxml/xmlsave.h>

#include <gnutls/abstract.h>

#include "include/libfints_messages.h"

/** TODO! **/
#define EBICS_GENEX_MAX_ENTRIES 64
#define EBICS_GENEX_MAX_NAME 128
#define EBICS_FATAL -1
#define EBICS_ERROR 1
#define EBICS_SUCCESS 0

struct EBICS_genex_document
{
  char name[EBICS_GENEX_MAX_NAME];
  xmlDocPtr document;
  xmlParserCtxtPtr parser;
  xmlXPathContextPtr xpath;
};

/**
 * Enum of available operations
 */
enum EBICS_MSG_Operations
{
  EBICS_MSG_OP_SELECT_CHOICE,
  EBICS_MSG_OP_UNIQUE_CHOICE,
  EBICS_MSG_OP_ADD_NODE,
  EBICS_MSG_OP_DEL_NODE,
  EBICS_MSG_OP_SET_FLAG,
  EBICS_MSG_OP_SET_STRING,
  EBICS_MSG_OP_SET_DATE,
  EBICS_MSG_OP_SET_INT,
  EBICS_MSG_OP_SET_UINT,
  EBICS_MSG_OP_SET_ATTRIBUTE,
  EBICS_MSG_OP_ADD_ATTRIBUTE,
  EBICS_MSG_OP_DEL_ATTRIBUTE,
  EBICS_MSG_OP_SUBCOMMAND,
  EBICS_MSG_OP_CLEAN,
  EBICS_MSG_OP_END
};

/* TODO
 */
enum EBICS_KEY_Type
{
  EBICS_KEY_NONE,
  EBICS_KEY_RSA_PUBLIC,
  EBICS_KEY_RSA_PRIVATE,
  EBICS_KEY_RSA_PAIR = EBICS_KEY_RSA_PUBLIC & EBICS_KEY_RSA_PRIVATE
};

/* TODO
 */
enum EBICS_KEY_HashAlgorithm
{
  EBICS_KEY_ALGORITHM_SHA256 = 0
};

/**
 * Store key(s).
 */
struct EBICS_Key
{
  enum EBICS_KEY_Type type;
  char name[128];
  gnutls_x509_privkey_t privatekey;
  gnutls_pubkey_t publickey;
};

/**
 * Stores all keys for a customer or bank
 */
struct EBICS_KeyList
{
  /** Length of the List */
  uint16_t length;
  /** List of keys */
  struct EBICS_Key *keys;
};

/* TODO
 */
struct EBICS_Date
{
  uint8_t day;
  uint8_t month;
  uint16_t year;
};

/**
 * typedef for subcommand funtion pointer
 */
typedef void (*EBICS_MSG_subcommand_fn)(void *cls, struct EBICS_genex_document *document);

struct EBICS_MSG_Spec
{
  /**
   * Format of the operation
   */
  enum EBICS_MSG_Operations operation;

  /**
   * (X)Path to the target element.
   */
  const char *xpath;

  union
  {
    struct
    {

      /** 
       * Every choice element is assigned
       * a index number; this values indicates
       * _which_ choice index is to be kept.
       * Other elements will be discarded from
       * the tree.
       *
       * XXX: are the index assigned in a
       * deterministic manner?  If not, this
       * value is bug-prone.
       */
      uint8_t choice;

    } select_choice;

    struct
    {
      /**
       * Never used; purge?
       */
    } select_unique;

    struct
    {

      /**
       * Node to add to the tree.
       */
      xmlNodePtr node;

    } add_node;

    struct
    {

      /**
       * Node to delete.  XXX why need the
       * pointer to the node?  Isn't it enough
       * to specify the XPath to the node to delete?
       * NOTE: this field looks UNUSED, and so might
       * better be removed from here.
       */
      xmlNodePtr node;

    } del_node;

    struct
    {

      /**
       * Flag to assign to the target element.
       */
      bool value;

    } set_flag;

    struct
    {

      /**
       * Test to assign to the target element.
       */
      xmlChar *value;

    } set_string;

    struct
    {

      /**
       * Unsigned integer to assign to the target element.
       */
      uint64_t value;

    } set_uint;

    struct
    {

      /**
       * Date to assign to the target element.
       */
      struct EBICS_Date date;

    } set_date;

    /* Set int */
    struct
    {

      /**
       * Value to assign to the target element.
       */
      int64_t value;

    } set_int;

    /* Set attribute */
    struct
    {

      /**
       * Name of the attribute.
       */
      xmlChar *name;

      /**
       * Value for the named attribute.
       */
      xmlChar *value;

    } attribute;

    struct
    {
      /**
       * Callback function. 
       */
      EBICS_MSG_subcommand_fn function;

      /**
       * General purpose closure for the callaback.
       */
      void *cls;

    } subcommand;

  } data;
};

/**
 * Selects a choice from the tree, unlinks the unselected choices 
 * and relinks the content to the choices parent node.
 *
 * @param xpath XPath expression that specifies _one_ CHOICES element.
 * @param choice Index of the CHOICE to select.
 * @return the operation to run.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_select_choice (const char *xpath, const uint8_t choice);


/** 
 *
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_unique_choice (const char *xpath);

struct EBICS_MSG_Spec
EBICS_MSG_op_set_uint (const char *xpath, const uint64_t value);

struct EBICS_MSG_Spec
EBICS_MSG_op_set_int (const char *xpath, const int64_t value);

struct EBICS_MSG_Spec
EBICS_MSG_op_set_flag (const char *xpath, const bool value);

struct EBICS_MSG_Spec
EBICS_MSG_op_set_string (const char *xpath, const char *value);

struct EBICS_MSG_Spec
EBICS_MSG_op_set_date (const char *xpath, const struct EBICS_Date date);

struct EBICS_MSG_Spec
EBICS_MSG_op_add_node (const char *xpath, const xmlNodePtr node);

struct EBICS_MSG_Spec
EBICS_MSG_op_del_node (const char *xpath);

struct EBICS_MSG_Spec
EBICS_MSG_op_set_attribute (const char *xpath, const char *value); 

struct EBICS_MSG_Spec
EBICS_MSG_op_add_attribute (const char *xpath, const char *name, const char *value); 

struct EBICS_MSG_Spec
EBICS_MSG_op_del_attribute (const char *xpath);


/**
 * Make a "subcommand" parsing instruction.
 *
 * @param the subcommand to invoke upon parsing.
 * @param cls general purpose closure for it.
 * @return the parsing instruction
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_subcommand (const EBICS_MSG_subcommand_fn subcmd,
                         void *cls); 

struct EBICS_MSG_Spec
EBICS_MSG_op_end ();

struct EBICS_MSG_Spec
EBICS_MSG_op_clean ();


void
EBICS_MSG_parse_spec(const struct EBICS_MSG_Spec *ops,
                     struct EBICS_genex_document *document);

#endif
