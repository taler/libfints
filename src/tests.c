/**
 * This file is part of libebics
 *
 * libfints is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3,
 * or (at your option) any later version.
 *
 * libebics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with libebics; see the file COPYING.
 * If not, see <http://www.gnu.org/license>
 */


#include "libebics.h"
#include <gnunet/platform.h>
#include <gnunet/gnunet_util_lib.h>

#define LOG(level,...) \
  EBICS_util_log_from (__LINE__, \
                       __FILE__, \
                       __func__, \
                       level, \
                       "messagedump", \
                       __VA_ARGS__)

struct EBICS_ARGS_build_header header_args = {
  .hostID = "EBIXHOST",
  .partnerID = "PARTNER1",
  .userID = "USER0001",
  .productName = "Unknown/Development",
  .languageCode = "en",
  .requestBankSignature = false
};

struct EBICS_ARGS_build_content_ini ini_args = {
  .partnerID = "PARTNER1",
  .userID = "USER0001"
};

struct EBICS_ARGS_build_content_hia hia_args = {
  .partnerID = "PARTNER1",
  .userID = "USER0001"
};

/**
 * This test merely instantiates documents
 * and checks them against their schemas.
 *
 * @return EBICS_ERROR on errors.
 */
int
main (int argc,
      char **argv)
{
  #define KEYS_DIR "./test_keys"

  struct EBICS_genex_document *msg;

  unsetenv ("XDG_DATA_HOME");
  unsetenv ("XDG_CONFIG_HOME");

  struct EBICS_UserKeyFiles key_filenames = {
    .es_key = "testEsKey",
    .enc_key = "testEncKey",
    .sig_key = "testSigKey",
  };

  if (EBICS_SUCCESS != EBICS_init_library (KEYS_DIR,
                                           &key_filenames)) 
  {
    LOG (EBICS_LOGLEVEL_ERROR,
	 "Lib not init\n");
    return EBICS_ERROR;
  }


  /**
   * INI
   */
  if (NULL == (msg = EBICS_generate_message_ini
      (&header_args,
       &ini_args)))
  {
    LOG (EBICS_LOGLEVEL_ERROR,
	 "Failed to instantiate INI message\n");
    return EBICS_ERROR;
  }

  util_dump_message (msg);
  GNUNET_free (msg);

  /**
   * HIA
   */
  if (NULL == (msg = EBICS_generate_message_hia
      (&header_args,
       &hia_args)))
  {
    LOG (EBICS_LOGLEVEL_ERROR,
	       "Failed to instantiate HIA message\n");
    return EBICS_ERROR;
  }
  
  util_dump_message (msg);
  GNUNET_free (msg);

  EBICS_close_library ();
  return EBICS_SUCCESS;
}
