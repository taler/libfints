/**
 * This file is part of libebics
 *
 * libebics is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3,
 * or (at your option) any later version.
 *
 * libebics is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with libebics; see the file COPYING.
 * If not, see <http://www.gnu.org/license>
 */

#include "util.h"
#include "xmlmessages.h"
#include "libxml/xpointer.h"
#include <gnutls/gnutls.h>
#include <gnutls/abstract.h>
#include <gnutls/crypto.h>
#include <gnutls/x509.h>
#include <assert.h>
#include <stdio.h>
#include <fts.h>
#include <zlib.h>
#include "libebics.h"
#include <gnunet/platform.h>
#include <gnunet/gnunet_util_lib.h>

#define LOG(level,...) \
  EBICS_util_log_from (__LINE__, \
                       __FILE__, \
                       __func__, \
                       level, \
                       "libebics", \
                       __VA_ARGS__)

static const struct GNUNET_OS_ProjectData libebics_pd = {
  .libname = "libebics",
  .project_dirname = "libebics",
  .binary_name = "NOBINARYINSTALLED",
  .env_varname = "EBICS_PREFIX",
  .base_config_varname = "EBICS_BASE_CONFIG",
  .bug_email = "taler@gnu.org",
  .homepage = "http://www.gnu.org/s/taler/",
  .config_file = "ebics.conf",
  .user_config_file = "~/.config/ebics.conf",
};

/**
 * List of (persistent) XML templates.
 */
static
struct EBICS_genex_document genexList[EBICS_GENEX_MAX_ENTRIES];


/**
 * List of (persistent) key material.  NOT static
 * as utility functions elsewhere need it.
 */
struct EBICS_Key keyList[EBICS_KEY_MAX_ENTRIES];


/**
 * Utility log function.
 *
 * @param level log level
 * @param msg log message
 */
static void
gnutlslog (int level, const char *msg)
{
  LOG (EBICS_LOGLEVEL_DEBUG,
       "GNUTLS[%d]: %s",
       level,
       msg);
}

/**
 * Initialize libgnutls
 *
 * @returns EBICS_SUCCESS on success, else EBICS_FATAL.
 */
static int
init_libgnutls()
{
  LOG (EBICS_LOGLEVEL_DEBUG,
       "Initializing 'libgnutls'\n");
  gnutls_global_init ();
  gnutls_global_set_log_function (&gnutlslog);
  gnutls_global_set_log_level (99);

  return EBICS_SUCCESS;
}


/**
 * Shut GNUTLS down.
 */
static void
free_libgnutls()
{
  /* NOTE: deprecated, see man page.  */
  gnutls_global_deinit();
}

/**
 * Initialize Libgcrypt
 *
 * @returns EBICS_SUCCESS on success, else EBICS_FATAL.
 */
static int
init_libgcrypt ()
{
  int rc;
  /* TODO: Manual advises to let the user init libgcrypt and 
   * for the library to only check the status 
   * (and possibly init if not done properly by user)
   */

  LOG (EBICS_LOGLEVEL_DEBUG,
       "Initializing 'libgcrypt'\n");
  /* Version check should be the very first call because it
     makes sure that important subsystems are intialized. */
  if (!gcry_check_version (GCRYPT_VERSION))
  {
    LOG (EBICS_LOGLEVEL_FATAL,
         "libgcrypt version mismatch\n");
    return (EBICS_FATAL);
  }
  
  /* Disable secure memory.  */
  if ((rc = gcry_control (GCRYCTL_DISABLE_SECMEM, 0)))
    LOG (EBICS_LOGLEVEL_ERROR,
             "Failed to set libgcrypt option %s: %s\n",
             "DISABLE_SECMEM",
             gcry_strerror (rc));

  /* ... If required, other initialization goes here.  */

  /* Tell Libgcrypt that initialization has completed.  */
  gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);
  return (EBICS_SUCCESS);
}

/**
 * deinit Libgcrypt.
 *
 * @return EBICS_SUCCESS if the operation succeeded,
 * EBICS_ERROR otherwise.
 */
static int
free_libgcrypt ()
{
  /* TODO or not ? */
  return EBICS_ERROR;
}

/**
 * Initialize Libxmlsec.
 *
 * @returns EBICS_SUCCESS on success, else EBICS_ERROR.
 */
static int
init_libxmlsec ()
{
  LOG (EBICS_LOGLEVEL_DEBUG,
       "Initializing 'libxmlsec'\n");

  /* Init libxml and libxslt libraries */
  LIBXML_TEST_VERSION;
  xmlLoadExtDtdDefaultValue = XML_DETECT_IDS | XML_COMPLETE_ATTRS;
  xmlSubstituteEntitiesDefault (1);
  xmlIndentTreeOutput = 1; 

  /* Init xmlsec library */
  if (xmlSecInit () < 0)
  {
    LOG (EBICS_LOGLEVEL_FATAL,
         "Error: xmlsec initialization failed.\n");
    return (EBICS_FATAL);
  }

  /* Check loaded library version */
  if (xmlSecCheckVersion() != 1)
  {
    LOG (EBICS_LOGLEVEL_FATAL,
         "Error: loaded xmlsec library version is not compatible.\n");
    return (EBICS_FATAL);
  }

  /* Init crypto library */
  if (xmlSecCryptoAppInit(NULL) < 0)
  {
    LOG (EBICS_LOGLEVEL_FATAL,
         "Error: crypto initialization failed.\n");
    return (EBICS_FATAL);
  }

  /* Init xmlsec-crypto library */
  if (xmlSecCryptoInit() < 0)
  {
    LOG (EBICS_LOGLEVEL_FATAL,
         "Error: xmlsec-crypto initialization failed.\n");
    return (EBICS_FATAL);
  }

  return (EBICS_SUCCESS);
}


/**
 * Deinit Libxmlsec.
 *
 * @return EBICS_SUCCESS if the operation succeeds,
 *         EBICS_ERROR otherwise.
 */
static int
free_libxmlsec ()
{
  #define RETURNIFERROR(func) \
    if(0!=func()) return EBICS_ERROR

  LOG (EBICS_LOGLEVEL_DEBUG,
       "Cleaning libxmlsec!\n");

  RETURNIFERROR (xmlSecGnuTLSShutdown);
  RETURNIFERROR (xmlSecGnuTLSAppShutdown);
  RETURNIFERROR (xmlSecCryptoShutdown);
  RETURNIFERROR (xmlSecShutdown);

#ifndef XMLSEC_NO_XSLT
  xsltCleanupGlobals ();
#endif

  return EBICS_SUCCESS;
}

/**
 * Init zlib.
 */
static int
init_zlib ()
{
  LOG (EBICS_LOGLEVEL_DEBUG,
       "zlib mock init\n");
  return EBICS_SUCCESS;
}


/**
 * Free zlib.
 */
static int
free_zlib ()
{
  return EBICS_SUCCESS;
}


/**
 * Load all keys.  For each filename given, the function
 * will try to import a x509 both private and public key.
 * However, it is not guaranteed that at the end of the
 * import all keys are imported priv&pub.
 *
 * @param keyList Will contain the initialized keys.
 * @param keyDir directory hosting the keys.
 * @param keyFiles names of the files with key material,
 *        ".pem" suffix NOT required.
 *        EBICS_KEY_MAX_ENTRIES is the maximum size admitted. 
 *
 * @returns EBICS_SUCCESS on success,
 *          EBICS_ERROR on non critical errors
 *          and EBICS_FATAL else.
 */
static int
init_keymaterial (struct EBICS_Key keyList[],
                  const char *keyDir,
                  const char *keyFiles[])
{
  int retv;
  int result = EBICS_SUCCESS;

  LOG (EBICS_LOGLEVEL_DEBUG,
       "Initializing key material\n");
  keyList[0].type = EBICS_KEY_NONE;
  
  for (int i = 0;
       i < EBICS_USER_KEYS_NUMBER;
       i++)
  {
    gnutls_x509_privkey_t privkey;
    gnutls_pubkey_t pubkey;
    size_t size;
    size_t key_size;
    struct EBICS_Key *key;
    FILE *f;
    char *key_content;

    size = snprintf (NULL,
                     0,
                     "%s/%s.pem",
                     keyDir,
                     keyFiles[i]);
    size++; /* account for 0-terminator */
    assert (0 != size);

    char filepath[size];
    size = snprintf (filepath,
                     size,
                     "%s/%s.pem",
                     keyDir,
                     keyFiles[i]);
    /* no 0-terminator in the size count,  although
     * string is 0-terminated  */
    assert (0 != size);

    LOG (EBICS_LOGLEVEL_DEBUG,
         "Loading '%s' at position %d",
         filepath,
         i);

    key = &keyList[i];
    keyList[i+1].type = EBICS_KEY_NONE;

    f = fopen (filepath, "rb"); /* rb: Read Binary */
    if (NULL == f)
    {
      LOG (EBICS_LOGLEVEL_ERROR,
           "Could not load %s",
           filepath);
      key->type = EBICS_KEY_NONE;
      continue;
    }

    fseek (f, 0, SEEK_END);
    size = ftell (f);
    /* bring the position again at the beginning  */
    fseek (f, 0, SEEK_SET);

    key_content = (char *) malloc (size);

    key_size = fread (key_content,
                      sizeof (char),
                      size,
                      f);

    assert (0 != key_size);
    fclose (f);
    LOG (EBICS_LOGLEVEL_DEBUG,
         "Size: %u",
         key_size);

    gnutls_datum_t rawkey = {
      key_content,
      size };

    gnutls_x509_privkey_init (&privkey);
    gnutls_pubkey_init (&pubkey);

    if (GNUTLS_E_SUCCESS == (
      retv = gnutls_x509_privkey_import (privkey,
                                         &rawkey,
                                         GNUTLS_X509_FMT_PEM)))
    {
      gnutls_privkey_t abspriv;

      key->privatekey = privkey;
      key->type |= EBICS_KEY_RSA_PRIVATE;

      LOG (EBICS_LOGLEVEL_DEBUG,
           "Found private key in %s!",
           filepath);


      /* convert x509 priv to abstract type first.  */
      gnutls_privkey_init (&abspriv);
      GNUNET_assert
        (GNUTLS_E_SUCCESS == (gnutls_privkey_import_x509 (abspriv,
                                                          privkey,
                                                          0)));
      if (GNUTLS_E_SUCCESS != (
        retv = gnutls_pubkey_import_privkey (pubkey,
                                             abspriv,
                                             /* XXX: POTENTIALLY WRONG; TO REVIEW.*/
                                             GNUTLS_KEY_DIGITAL_SIGNATURE,
                                             0))) // docs dictates 0 here.
      {
      
        LOG (EBICS_LOGLEVEL_ERROR,
             "Could not extract public key from"
             " private at %s. GnuTLS Error: %s",
             filepath,
             gnutls_strerror (retv));
             return EBICS_ERROR;
      }
      else
      {
        LOG (EBICS_LOGLEVEL_DEBUG,
             "Succefully extracted public key from private at %s\n",
             filepath);
        gnutls_privkey_deinit (abspriv);
        key->publickey = pubkey;
        key->type |= EBICS_KEY_RSA_PUBLIC;
      }
    }
    else
    {
      LOG (EBICS_LOGLEVEL_ERROR,
           "Could not import private key at %s,"
           "trying importing public key: %s",
           filepath,
           gnutls_strerror (retv));
    }

    /**
     * The policy prioritizes public keys extracted from privs.
     * Therefore whenever a private key got detected in the current
     * file, then the associated public key is extracted from _that_
     * private key, discarding any other potential public key appended
     * to the file.
     */
    if (0 == (EBICS_KEY_RSA_PRIVATE & key->type))
    {
      if (GNUTLS_E_SUCCESS == (
          retv = gnutls_pubkey_import (pubkey,
                                       &rawkey,
                                       GNUTLS_X509_FMT_PEM)))
      {
        LOG (EBICS_LOGLEVEL_DEBUG,
             "Found public key in %s!",
             filepath);
        key->publickey = pubkey;
        key->type |= EBICS_KEY_RSA_PUBLIC;
      }
      else
      {
        LOG (EBICS_LOGLEVEL_ERROR,
        "Could not import publickey from file %s. GnuTLS Error: %s",
        gnutls_strerror (retv));
        return EBICS_ERROR;
      }
    }

    strncpy (key->name,
             keyFiles[i],
             EBICS_KEY_MAX_NAME);

    GNUNET_free (key_content);
  }

  return EBICS_SUCCESS;
}


/**
 * "Free" imported keys.
 *
 * @param keyList list of imported keys.  Usually
 *        a global object that lives along all the
 *        library's life.
 *
 * @return EBICS_SUCCESS or EBICS_ERROR
 */
static int
free_keymaterial (struct EBICS_Key keyList[])
{
  LOG (EBICS_LOGLEVEL_DEBUG, "Freeing key material");

  int retv;
  int result = EBICS_SUCCESS;
  
  for (int i = 0; i < EBICS_KEY_MAX_ENTRIES;  i++)
  {
    if (keyList[i].type == EBICS_KEY_NONE)
      continue;
    struct EBICS_Key *key = &keyList[i];
    LOG (EBICS_LOGLEVEL_DEBUG, "Freeing Key with name: %s", key->name);
    gnutls_x509_privkey_deinit (key->privatekey);
    key->privatekey = NULL;
    gnutls_pubkey_deinit (key->publickey);
    key->publickey = NULL;
  }
}

/**
 * Load and parse all needed genex-style documents and create xpath contexts.
 *
 * @returns EBICS_SUCCESS on success, else EBICS_ERROR.
 */
static int
init_genex_documents
  (struct EBICS_genex_document genexList[],
   const char *genex_schemas_dir,
   const char *genexFiles[])
{
  LOG (EBICS_LOGLEVEL_DEBUG,
       "Initializing 'genex_documents'");

  int result = EBICS_SUCCESS;
  
  for (int i = 0;
       i < EBICS_GENEX_MAX_ENTRIES && genexFiles[i] != NULL;
       i++)
  {
    size_t size;
    struct EBICS_genex_document *genex;
    xmlParserCtxtPtr parser;
    xmlDocPtr doc;
    xmlXPathContextPtr xpathCtxPtr;

    size = snprintf (NULL,
                     0,
                     "%s/%s",
                     genex_schemas_dir,
                     genexFiles[i]);
    size++;
    GNUNET_assert (0 != size);

    char filepath[size];
    size = snprintf (filepath,
                     size,
                     "%s/%s",
                     genex_schemas_dir,
                     genexFiles[i]);

    GNUNET_assert (0 != size);
    LOG (EBICS_LOGLEVEL_DEBUG,
         "Loading '%s' at position %d",
         filepath,
         i);
    genex = &genexList[i];
    genexList[i+1].document = NULL;

    parser = xmlNewParserCtxt ();
    doc = xmlCtxtReadFile (parser,
                           filepath,
                           "utf-8",
                           XML_PARSE_NOBLANKS);
    if (doc == NULL)
    {
      LOG (EBICS_LOGLEVEL_ERROR,
           "Could not parse document: %s",
           filepath);
      result = EBICS_ERROR;
    }
    xpathCtxPtr = xmlXPathNewContext (doc);
    if (NULL == xpathCtxPtr)
    {
      LOG (EBICS_LOGLEVEL_ERROR,
           "Could not create xmlXPathCtxPtr for document: %s",
           filepath);
      result = EBICS_ERROR;
    }
    xmlXPathRegisterNs (xpathCtxPtr,
                        "ebics",
                        "urn:org:ebics:H004");
    xmlXPathRegisterNs (xpathCtxPtr,
                        "esig",
                        "http://www.ebics.org/S001");
    xmlXPathRegisterNs (xpathCtxPtr,
                        "schema",
                        "http://www.w3.org/2001/XMLSchema");
    xmlXPathRegisterNs (xpathCtxPtr,
                        "ds",
                        "http://www.w3.org/2000/09/xmldsig#");

    genex->document = doc;
    genex->parser = parser;
    genex->xpath = xpathCtxPtr;
    strncpy (genex->name,
             genexFiles[i],
             EBICS_GENEX_MAX_NAME);
  }
  return result;
}


/**
 * Free the master base XML documents; usually maintained
 * in a global object that gets NEVER destroyed throughout
 * all the library's life.
 *
 * @param genexList list of documents to free.
 * @return EBICS_SUCCESS or EBICS_ERROR.
 */
static int
free_genex_documents (struct EBICS_genex_document genexList[])
{
  LOG (EBICS_LOGLEVEL_DEBUG, "Freeing 'genex_documents'");

  int result = EBICS_SUCCESS;
  
  for (int i = 0; i < EBICS_GENEX_MAX_ENTRIES && genexList[i].document != NULL; i++)
  {
    struct EBICS_genex_document *genex = &genexList[i];
    
    xmlXPathRegisteredNsCleanup(genex->xpath);
    xmlXPathFreeContext(genex->xpath);
    xmlFreeParserCtxt(genex->parser);
    xmlFreeDoc(genex->document);
  }
}

/**
 * Initializes Libebics.  Init all the dependencies,
 * as well as it allocates the "genex" templates to
 * be copied and instantiated during the library life.
 *
 * @param key_dir directory where keys to be imported
 *        are located.
 * @param key_files array of filenames indicating PEM
 *        formatted files on disk; last entry must be NULL.
 *        ".pem" suffix NOT required.
 *        EBICS_KEY_MAX_ENTRIES is the maximum size admitted. 
 *
 * @return EBICS_SUCCESS or EBICS_ERROR.
 **/
int
EBICS_init_library (const char *key_dir,
                    const struct EBICS_UserKeyFiles *key_files)
{
  int retv;
  const char *keyFiles[EBICS_USER_KEYS_NUMBER];
  
  /**
   * Directory where XML templates are stored.
   */
  char *data_dir;

  const char *genexFilenames[EBICS_GENEX_MAX_ENTRIES] = {
    "ebicsRequest.xml", 
    "SignaturePubKeyOrderData.xml", 
    "HIARequestOrderData.xml",
    "ebicsUnsecuredRequest.xml", 
    "ebicsNoPubKeyDigestsRequest.xml",
    NULL};

  xmlInitParser ();

  retv = init_libgcrypt ();
  if (retv)
    return retv;

  retv = init_libxmlsec ();
  if (retv)
    return retv;

  retv = init_libgnutls ();
  if (retv)
    return retv;

  retv = init_zlib ();
  if (retv)
    return retv;

  GNUNET_OS_init (&libebics_pd);

  /* Challenge: get the "shared" directory. */
  if (NULL == (data_dir = GNUNET_OS_installation_get_path
      (GNUNET_OS_IPK_DATADIR)))
    return EBICS_ERROR;
  if (EBICS_SUCCESS != init_genex_documents
      (genexList,
       data_dir,
       genexFilenames))
  {
    LOG (EBICS_LOGLEVEL_FATAL,
         "Could not initialize all the genex documents\n");
    return EBICS_ERROR;
  }

  keyFiles[EBICS_USER_ES_KEY] = key_files->es_key;
  keyFiles[EBICS_USER_ENC_KEY] = key_files->enc_key;
  keyFiles[EBICS_USER_SIG_KEY] = key_files->sig_key;

  if (EBICS_SUCCESS != init_keymaterial
      (keyList,
       key_dir,
       keyFiles))
  {
    GNUNET_break (0);
    return EBICS_ERROR;
  }

  GNUNET_free (data_dir);
  return EBICS_SUCCESS;
}


/**
 * Deinit / deallocate libebics and all the dependencies.
 *
 * @return EBICS_SUCCESS or EBICS_ERROR
 */
int
EBICS_close_library ()
{
  int retv;

  retv = free_libgcrypt ();
  if (retv)
    return (retv);

  retv = free_libxmlsec ();
  if (retv)
    return (retv);

  retv = free_zlib ();
  if (retv)
    return (retv);
  
  free_libgnutls ();
  xmlCleanupParser ();

  free_genex_documents (genexList);
  free_keymaterial (keyList);
}

/**
 * Allocate a fresh genex document.
 *
 * @param type_index index pointing to one genex
 * base documents into the global genex array.
 * @return pointer to a freshly allocated copy of
 * the base document; to be freed by the caller.
 */
struct EBICS_genex_document *
get_genex_instance (unsigned int type_index)
{
  struct EBICS_genex_document *ret;

  ret = GNUNET_new (struct EBICS_genex_document);
  memcpy (ret,
          &genexList[type_index],
          sizeof (struct EBICS_genex_document));

  return ret;
}

/**
 * Customize a bunch of standard values in the tree (including
 * the HostID).
 *
 * @param header_args TODO
 * @param ini_args TODO
 * @return pointer to a freshly allocated document, NULL upon errors.
 */
struct EBICS_genex_document *
EBICS_generate_message_ini
  (struct EBICS_ARGS_build_header *header_args,
   struct EBICS_ARGS_build_content_ini *ini_args)
{

  struct EBICS_genex_document *instance;

  if (NULL == (instance = get_genex_instance
    (EBICS_INI_WRAPPER_TEMPLATE)))
  {
    LOG (EBICS_ERROR,
         "Could not allocate genex instance\n");
    return NULL;
  }
  LOG (EBICS_LOGLEVEL_DEBUG,
       "Picked base document %s\n",
       instance->name);

  struct EBICS_MSG_Spec spec[] = {

    /* Notably, it sets the HostID in the request document.  */
    EBICS_MSG_op_subcommand
      (EBICS_build_header_ebicsUnsecuredRequest,
       header_args),

    /* Set a bunch of nodes (mostly strings)
     * taking values from 'ini_args'.  */
    EBICS_MSG_op_subcommand (EBICS_build_content_ini,
                             ini_args),
    EBICS_MSG_op_clean (),
    EBICS_MSG_op_end ()
  };

  EBICS_MSG_parse_spec (spec,
                        instance);

  return instance;
}


/**
 * Generator of HIA messages.
 *
 * @param header_args values for the HIA header
 *        (typically ebicsUnsecuredRequest).
 * @param hia_args values for the HIA payload.
 * @return pointer to a freshly allocated document,
 *         NULL upon errors.
 */
struct EBICS_genex_document *
EBICS_generate_message_hia
  (struct EBICS_ARGS_build_header *header_args,
   struct EBICS_ARGS_build_content_hia *hia_args)
{

  struct EBICS_genex_document *instance;

  if (NULL == (instance = get_genex_instance
    (EBICS_HIA_WRAPPER_TEMPLATE)))
  {
    LOG (EBICS_ERROR,
         "Could not allocate genex instance\n");
    return NULL;
  }


  struct EBICS_MSG_Spec spec[] = {

    EBICS_MSG_op_subcommand
      (EBICS_build_header_ebicsUnsecuredRequest,
       header_args),
    EBICS_MSG_op_subcommand (EBICS_build_content_hia,
                             hia_args),
    EBICS_MSG_op_clean (),
    EBICS_MSG_op_end ()
  };

  EBICS_MSG_parse_spec (spec,
                        instance);
  return instance;
}

/**
 * Generator of HPB messages.
 *
 * @param header_args TODO
 * @param auth_args TODO
 * @return pointer to a freshly allocated document, NULL upon errors.
 */
struct EBICS_genex_document *
EBICS_generate_message_hpb (struct EBICS_ARGS_build_header *header_args,
                            struct EBICS_ARGS_build_auth *auth_args)
{

  struct EBICS_genex_document *instance;

  if (NULL == (instance = get_genex_instance
    (0))) // FIXME
  {
    LOG (EBICS_ERROR,
         "Could not allocate genex instance\n");
    return NULL;
  }

  struct EBICS_MSG_Spec foo[] = {

    EBICS_MSG_op_subcommand (EBICS_build_header_ebicsNoPubKeyDigestsRequest,
                             header_args),
    EBICS_MSG_op_set_string ("//ebics:OrderDetails//ebics:OrderType",
                             "HPB"),
    EBICS_MSG_op_del_node ("//ds:X509Data"),
    EBICS_MSG_op_subcommand (EBICS_build_auth_signature,
                             auth_args),
    EBICS_MSG_op_clean (),
    EBICS_MSG_op_end ()
  };

  EBICS_MSG_parse_spec(foo,
                       instance);
}

/**
 * Generator of CAMT.053 messages.
 *
 * @param header_args TODO
 * @param auth_args TODO
 * @param camt053_args TODO
 * @return pointer to a freshly allocated document, NULL upon errors.
 */
struct EBICS_genex_document *
EBICS_generate_message_camt053 (struct EBICS_ARGS_build_header *header_args,
                                struct EBICS_ARGS_build_auth *auth_args,
                                struct EBICS_ARGS_build_content_camt053 *camt053_args)
{

  struct EBICS_genex_document *instance;

  if (NULL == (instance = get_genex_instance
    (0))) //FIXME
  {
    LOG (EBICS_ERROR,
         "Could not allocate genex instance\n");
    return NULL;
  }

  struct EBICS_MSG_Spec foo[] = {
    EBICS_MSG_op_subcommand (EBICS_build_header_ebicsRequest,
                             header_args),
    EBICS_MSG_op_subcommand (EBICS_build_content_camt053,
                             camt053_args),
    EBICS_MSG_op_subcommand (EBICS_build_bankPubKeyDigest,
                             auth_args),
    EBICS_MSG_op_subcommand (EBICS_build_auth_signature,
                             auth_args),
    EBICS_MSG_op_clean (),
    EBICS_MSG_op_end ()
  };

  EBICS_MSG_parse_spec (foo,
                        instance);
  return instance;
}
