/*
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
*/
#define _GNU_SOURCE 

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>
#include <assert.h>
#include "xmlproto.h"
#include "util.h"

/**
 * Local LOG for xmlproto component.
 */
#define LOG(level,...) EBICS_util_log_from (__LINE__,__FILE__,__func__,level, "xmlproto",__VA_ARGS__)

#define EBICS_debug_xmlpath(node) \
                do {\
                  xmlChar* path = xmlGetNodePath(node); \
                  LOG(EBICS_LOGLEVEL_DEBUG,"Name: %s Path: %s",node->name, path); \
                  xmlFree(path); \
                } while (0)



/**
 * Select a choice from the tree.
 * Selects a choice from the tree, unlinks the unselected choices 
 * and relinks the content to the choices parent node.
 *
 * @param xpath XPath expression that specifies _one_ CHOCIES element.
 * @param choice Index of the CHOICE to select.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_select_choice (const char *xpath, const uint8_t choice) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_SELECT_CHOICE, 
      .xpath = xpath, 
      .data.select_choice.choice = choice
    };
  return result;
};

/**
 * Select all choices above the specified node.
 * Selects all choices so that the specified element is in the resulting tree.
 *
 * @param xpath XPath expression that specifies the element which should be contained in the resulting tree.
 *
 * @return MSG_Spec struct describing the operation.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_unique_choice (const char *xpath) 
{
  struct EBICS_MSG_Spec result = {
      .operation = EBICS_MSG_OP_UNIQUE_CHOICE, 
      .xpath = xpath,
  };
  return result;
};

/**
 * Set content of node to unsigned integer.
 * Set content of node to the utf-8 string representation of a unsigned integer without leading zeros.
 *
 * @param xpath XPath expression that specifies the node.
 * @param value Value to set for the content of the node.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_set_uint (const char *xpath, const uint64_t value) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_SET_UINT, 
      .xpath = xpath,
      .data.set_uint.value = value
    };
  return result;
};

/**
 * Set content of node to integer.
 * Set content of node to the utf-8 string representation of a integer without leading zeros.
 *
 * @param xpath XPath expression that specifies the node.
 * @param value Value to set for the content of the node.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_set_int (const char *xpath, const int64_t value) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_SET_INT, 
      .xpath = xpath,
      .data.set_int.value = value
    };
  return result;
};

/**
 * Set a flag node.
 * Set a flag node, either deleting the node or unsetting content and keeping node.
 *
 * @param xpath XPath expression that specifies the node.
 * @param value Boolean value of flag status.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_set_flag (const char *xpath, const bool value) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_SET_FLAG, 
      .xpath = xpath,
      .data.set_flag.value = value
    };
  return result;
};

/**
 * Set content of node to date.
 * Set content of node to the utf-8 string representation of the given date.
 *
 * @param xpath XPath expression that specifies the node.
 * @param value UTF-8 String to set content to.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_set_date (const char *xpath, const struct EBICS_Date date) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_SET_DATE,
      .xpath = xpath,
      .data.set_date.date = date
    };
  return result;
};

/**
 * Set content of node to utf-8 string.
 * Set content of node to the given utf-8 string.
 *
 * @param xpath XPath expression that specifies the node.
 * @param value UTF-8 String to set content to.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_set_string (const char *xpath, const char *value) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_SET_STRING,
      .xpath = xpath,
      .data.set_string.value = xmlCharStrdup(value)
    };
  return result;
};

/**
 * Set content of attribute to utf-8 string.
 * Set content of attribute of node to the given utf-8 string.
 *
 * @param xpath XPath expression that specifies the node.
 * @param value UTF-8 String to set content to.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_set_attribute (const char *xpath, const char *value) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_SET_ATTRIBUTE,
      .xpath = xpath,
      .data.attribute.value = xmlCharStrdup(value)
    };
  return result;
};

struct EBICS_MSG_Spec
EBICS_MSG_op_add_attribute (const char *xpath, const char *name, const char *value) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_ADD_ATTRIBUTE,
      .xpath = xpath,
      .data.attribute.name = xmlCharStrdup(name),
      .data.attribute.value = xmlCharStrdup(value)
    };
  return result;
};

struct EBICS_MSG_Spec
EBICS_MSG_op_del_attribute (const char *xpath) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_DEL_ATTRIBUTE,
      .xpath = xpath,
    };
  return result;
};

/**
 * Add given node as a child to node.
 * Add to the node specified by the xpath expression the given node as a child.
 *
 * @param xpath XPath expression that specifies the node.
 * @param node Node that will be added as a child.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_add_node (const char *xpath, const xmlNodePtr node) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_ADD_NODE,
      .xpath = xpath,
      .data.add_node.node = node
    };
  return result;
};

/**
 * TODO
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_del_node (const char *xpath) 
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_DEL_NODE,
      .xpath = xpath,
    };
  return result;
};

/**
 * Make a "subcmd" operation.
 *
 * @param subcmd the function that the interpreter
 *        will call passing the rest of the arguments to.
 * @param cls closure for the sub-command.
 * @param target the document that will absorb the changes
 *        carried out by the sub-command *IF* there is no
 *        target document in the closure.
 * @return a command runnable by the interpreter.
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_subcommand (EBICS_MSG_subcommand_fn subcmd,
                         void *cls)
{
  struct EBICS_MSG_Spec result = {
    .operation = EBICS_MSG_OP_SUBCOMMAND,
    .data.subcommand.function = subcmd,
    .data.subcommand.cls = cls
  };

  return result;
};

/**
 * Cleanup marker for MSG_Spec array
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_clean ()
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_CLEAN
    };
  return result;
};

/**
 * End marker for MSG_Spec array
 */
struct EBICS_MSG_Spec
EBICS_MSG_op_end ()
{
  struct EBICS_MSG_Spec result = 
    {
      .operation = EBICS_MSG_OP_END
    };
  return result;
};

/**
 * Relink content of a CHOICE node to its content parent.
 *
 * @param choice The CHOICE node to relink.
 * @param doc the xml document context (is this needed?)
 *
 * @return the parent of the relinked node
 */
static xmlNodePtr
relink_choice(xmlNodePtr choice, xmlDocPtr doc)
{
  xmlNodePtr current = NULL;
  xmlNodePtr choice_base = NULL;
  xmlNodePtr choice_element = NULL;
  xmlNodePtr choice_parent = NULL;

  LOG(EBICS_LOGLEVEL_INFO,"relink_choice: Looking for parent.");
  for (choice_base = choice;
       choice_base && !xmlStrEqual(choice_base->name, BAD_CAST "CHOICES");
       choice_base = choice_base->parent)
  {
    if (xmlStrEqual(choice_base->name, BAD_CAST "CHOICE"))
    {
      choice_element = choice_base;
      LOG(EBICS_LOGLEVEL_INFO,"relink_choice: found choice element: %s", choice_element->name);
    }
    LOG(EBICS_LOGLEVEL_INFO,"relink_choice: looking at %s", choice_base->name);
    EBICS_debug_xmlpath(choice_base);
  }

  if (!choice_base || !choice_element || !xmlStrEqual(choice_base->name, BAD_CAST "CHOICES"))
  {
    return NULL;
  }
  choice_parent = choice_base->parent;
  LOG(EBICS_LOGLEVEL_INFO,"relink_choice: parents name: %s", choice_parent->name);

  LOG(EBICS_LOGLEVEL_INFO,"relink_choice: relinking children.");
  current = xmlFirstElementChild(choice_element);
  while (current != NULL)
  {
    xmlNodePtr next_sibling = xmlNextElementSibling(current);
    LOG(EBICS_LOGLEVEL_INFO,"relink_choice: \trelinking %s to %s", current->name, choice_parent->name);
    xmlUnlinkNode(current);
    assert(xmlAddChild(choice_parent, current));
    current = next_sibling;
  }

  xmlUnlinkNode(choice_base);
  xmlFreeNode(choice_base);

  return choice_parent;
}


/** 
 * Remove lfts_opt optional marker above a used element.
 * If not called on all used elements with a lfts_optional attribute or
 * with a parent with the lfts_optional attribute set to true, elements containing data
 * could be dropped in the remove_lfts_optional function.
 *
 * @param node Node containing lfts_optional attribute or with parent containing it.
 * @param doc Document pointer.
 */
static void
remove_opt_attribute (xmlNodePtr node,
		      xmlDocPtr doc)
{
  xmlNodePtr current;

  for (current = node;
       current && current != xmlDocGetRootElement (doc);
       current = current->parent)
  {
    LOG (EBICS_LOGLEVEL_INFO,
	 "Looking at %s\n",
	 current->name);
    if (0 <= xmlUnsetProp (current,
                           BAD_CAST "lfts_optional"))
      LOG (EBICS_LOGLEVEL_INFO,
           "Found opt, removing\n");
  }
}

/** 
 * Remove all remaining elements with the lfts_optional attribute set to true.
 * Call when all needed elements are set within the document and remove all additional elements
 * with the lfts_optional attribute set to true.
 *
 * @param xpathCtxPtr Pointer to the XPath context.
 * @param doc Pointer to the base document->
 */
static void
remove_lfts_optional_nodes (struct EBICS_genex_document *document)
{
  int i;
  xmlNodePtr node;
  xmlXPathContextPtr xpathCtxPtr;
  xmlXPathObjectPtr xpathObjPtr;
  xmlNodeSetPtr nodeset;

  xpathCtxPtr = document->xpath;
  xpathObjPtr = xmlXPathEvalExpression
    (BAD_CAST "//*[@lfts_optional='True']",
     xpathCtxPtr);

  assert (NULL != xpathObjPtr);
  assert (NULL != xpathObjPtr->nodesetval);
  nodeset = xpathObjPtr->nodesetval;

  LOG (EBICS_LOGLEVEL_INFO,
       "remove_lfts_opt: Number of Nodes found: %d\n",
       nodeset->nodeNr);

  for (i = (nodeset->nodeNr);
       i > 0;
       i--)
  {
    node = nodeset->nodeTab[i-1];

    LOG (EBICS_LOGLEVEL_INFO,
         "\tFound: %s\n",
         node->name);

    xmlUnlinkNode (node);
    xmlFreeNode (node);
    nodeset->nodeTab[i-1] = NULL;
  }

  xmlXPathFreeObject (xpathObjPtr);
}

/**
 * Remove all remaining "ANY" nodes.
 *
 */
static void
remove_any_nodes (struct EBICS_genex_document *document)
{
  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlNodePtr node;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression(BAD_CAST "//schema:ANY", xpathCtxPtr);
  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;
  LOG(EBICS_LOGLEVEL_INFO,"remove_any_nodes: Number of Nodes found: %d",nodeset->nodeNr);
  int i;
  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    node = nodeset->nodeTab[i-1];
    LOG(EBICS_LOGLEVEL_INFO,"\tFound: %s", node->name);
    xmlUnlinkNode(node);
    xmlFreeNode(node);
    nodeset->nodeTab[i-1] = NULL;
  }
  xmlXPathFreeObject(xpathObjPtr);
}

/**
 * Process a choice operation.
 *
 */
static void
process_choice (const struct EBICS_MSG_Spec *operation,
                struct EBICS_genex_document *document)
{
  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlDocPtr doc = document->document;
  /*
  xmlChar *choice = malloc(sizeof(xmlChar) * 32);
  xmlStrPrintf(choice, 32*sizeof(xmlChar), "/msg:CHOICE[@index=%u]", operation->data.select_choice.choice);
  */
  char *choice;
  assert( 0 < asprintf(&choice, "/schema:CHOICE[@index=%u]", operation->data.select_choice.choice));
  xmlChar *base = xmlStrdup(BAD_CAST operation->xpath);
  xmlChar *result = xmlStrcat(base, BAD_CAST choice);
  free(choice);

  LOG(EBICS_LOGLEVEL_INFO,"process_choice: xpath expresion: %s", result);
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression(result, xpathCtxPtr);
  xmlFree(result);
  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;
  assert(1 == nodeset->nodeNr);
  xmlNodePtr node = nodeset->nodeTab[0];
  xmlXPathFreeObject(xpathObjPtr);
  relink_choice(node, doc);
}

/**
 * Parse a unique choice operation.
 * 
 */
static void
process_unique (const struct EBICS_MSG_Spec *operation,
                struct EBICS_genex_document *document)
{

  xmlXPathContextPtr xpathCtxPtr;
  xmlDocPtr doc;
  xmlXPathObjectPtr xpathObjPtr; 
  xmlNodeSetPtr nodeset;
  xmlNodePtr node;

  xpathCtxPtr = document->xpath;
  doc = document->document;

  xpathObjPtr = xmlXPathEvalExpression
    (BAD_CAST operation->xpath,
     xpathCtxPtr);

  assert (NULL != xpathObjPtr);
  assert (NULL != xpathObjPtr->nodesetval);

  nodeset = xpathObjPtr->nodesetval;
  assert (1 == nodeset->nodeNr);
  node = nodeset->nodeTab[0];
  xmlXPathFreeObject (xpathObjPtr);

  while (node && node != xmlDocGetRootElement (doc))
  {
    LOG (EBICS_LOGLEVEL_INFO,
         "process_unique: Relinking %s",
         node->name);
    node = relink_choice (node, doc);
  }
}

/**
 * Parse a set uint operation.
 */
static void
process_uint (const struct EBICS_MSG_Spec *operation,
              struct EBICS_genex_document *document)
{
  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlDocPtr doc = document->document;
  uint32_t i;
  char *content;
  xmlNodePtr node;
  uint64_t value = operation->data.set_uint.value;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression(BAD_CAST operation->xpath, xpathCtxPtr);
  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG(EBICS_LOGLEVEL_INFO,"process_string:"
              "path: %s Xpathresult: %p,"
              "nodeset: %p, nodeNr: %u", 
              operation->xpath, xpathObjPtr, 
              nodeset, nodeset->nodeNr);
  assert(0 < asprintf(&content, "%" PRIu64, value));
  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    LOG(EBICS_LOGLEVEL_INFO,"process_uint: setting value: %"PRIu64"",value);
    node = nodeset->nodeTab[i-1];
    xmlNodeSetContent(node, BAD_CAST content);
    remove_opt_attribute(node, doc);
  }
  xmlXPathFreeObject(xpathObjPtr);
  free(content);
}

/**
 * Parse a set int operation.
 */
static void
process_int (const struct EBICS_MSG_Spec *operation,
             struct EBICS_genex_document *document)
{
  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlDocPtr doc = document->document;
  uint32_t i;
  char *content;
  xmlNodePtr node;
  int64_t value = operation->data.set_int.value;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression(BAD_CAST operation->xpath, xpathCtxPtr);
  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG(EBICS_LOGLEVEL_INFO,"process_string: \
              xpath: %s Xpathresult: %p, \
              nodeset: %p, nodeNr: %u", 
              operation->xpath, xpathObjPtr, 
              nodeset, nodeset->nodeNr);
  assert(0 < asprintf(&content, "%" PRId64, value));
  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    LOG(EBICS_LOGLEVEL_INFO,"process_int: setting value: %"PRId64"",value);
    node = nodeset->nodeTab[i-1];
    xmlNodeSetContent(node, BAD_CAST content);
    remove_opt_attribute(node, doc);
  }
  xmlXPathFreeObject(xpathObjPtr);
  free(content);
}

/**
 * Parse a set flag operation.
 */
static void
process_flag (const struct EBICS_MSG_Spec *operation,
              struct EBICS_genex_document *document)
{
  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlDocPtr doc = document->document;
  uint32_t i;
  xmlNodePtr node;
  bool flag = operation->data.set_flag.value;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression(BAD_CAST operation->xpath, xpathCtxPtr);
  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;
  LOG(EBICS_LOGLEVEL_INFO,"process_string: \
              xpath: %s Xpathresult: %p, \
              nodeset: %p, nodeNr: %u", 
              operation->xpath, xpathObjPtr, 
              nodeset, nodeset->nodeNr);

  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    node = nodeset->nodeTab[i-1];
    if (flag)
    {
      xmlNodeSetContent(node, BAD_CAST "");
      remove_opt_attribute(node, doc);
    }
    else
    {
      xmlUnlinkNode(node);
      xmlFreeNode(node);
    }
  }
  xmlXPathFreeObject(xpathObjPtr);
}

/** Parse a set string operation.
 */
static void
process_string (const struct EBICS_MSG_Spec *operation,
                struct EBICS_genex_document *document)
{
  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlDocPtr doc = document->document;
  xmlChar *text = operation->data.set_string.value;
  xmlNodePtr node;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression(BAD_CAST operation->xpath, xpathCtxPtr);
  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG(EBICS_LOGLEVEL_INFO,"process_string: \
              xpath: %s Xpathresult: %p, \
              nodeset: %p, nodeNr: %u", 
              operation->xpath, xpathObjPtr, 
              nodeset, nodeset->nodeNr);


  int i;
  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    LOG(EBICS_LOGLEVEL_INFO,"process_string: setting text: %s",text);
    node = nodeset->nodeTab[i-1];
    xmlNodeSetContent(node, text);
    remove_opt_attribute(node, doc);

  }
  xmlXPathFreeObject(xpathObjPtr);
  free(text);
}

/** Parse a set date operation.
 */
static void
process_date (const struct EBICS_MSG_Spec *operation,
              struct EBICS_genex_document *document)
{
  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlDocPtr doc = document->document;
  xmlNodePtr node;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression(BAD_CAST operation->xpath, xpathCtxPtr);
  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG(EBICS_LOGLEVEL_INFO,"process_date: \
              xpath: %s Xpathresult: %p, \
              nodeset: %p, nodeNr: %u", 
              operation->xpath, xpathObjPtr, 
              nodeset, nodeset->nodeNr);


  int i;
  const struct EBICS_Date date = operation->data.set_date.date;
  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    node = nodeset->nodeTab[i-1];
    char text[11];
    int ret = snprintf(text, 11, "%04u-%02u-%02u", date.year, date.month, date.day);
    assert(11 > ret && ret > 0);
    xmlNodeSetContent(node, (xmlChar*) text);
    remove_opt_attribute(node, doc);

  }
  xmlXPathFreeObject(xpathObjPtr);
}

/**
 * Parse a set attribute operation.
 *
 * @param operation the operation to run
 * @param document the output document.
 */
static void
process_attribute_set (const struct EBICS_MSG_Spec *operation,
                       struct EBICS_genex_document *document)
{

  int i;
  xmlNodePtr node;

  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlDocPtr doc = document->document;
  xmlChar *text = operation->data.attribute.value;
  xmlChar *name = operation->data.attribute.name;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression
    (BAD_CAST operation->xpath,
     xpathCtxPtr);
  assert (NULL != xpathObjPtr);
  assert (NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG (EBICS_LOGLEVEL_INFO,
       "process_attribute_set: \
       xpath: %s Xpathresult: %p, \
       nodeset: %p, nodeNr: %u", 
       operation->xpath,
       xpathObjPtr, 
       nodeset,
       nodeset->nodeNr);

  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    LOG (EBICS_LOGLEVEL_INFO,
         "process_attribute_set: setting text: %s",
         text);
    node = nodeset->nodeTab[i-1];
    xmlNodeSetContent(node, text);
    remove_opt_attribute(node, doc);

  }
  xmlXPathFreeObject (xpathObjPtr);
  free (name);
  free (text);
}


/**
 * Delete a (XML) attribute from the document.
 *
 * @param operation the delete-attribute operation
 * @param document the output document
 */
static void
process_attribute_del (const struct EBICS_MSG_Spec *operation,
                       struct EBICS_genex_document *document)
{
  int i;
  xmlNodePtr node;
  xmlXPathContextPtr xpathCtxPtr = document->xpath;

  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression
    (BAD_CAST operation->xpath,
     xpathCtxPtr);
  assert (NULL != xpathObjPtr);
  assert (NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG (EBICS_LOGLEVEL_INFO,
       "process_attribute_del: \
       xpath: %s Xpathresult: %p, \
       nodeset: %p, nodeNr: %u", 
       operation->xpath,
       xpathObjPtr, 
       nodeset,
       nodeset->nodeNr);

  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    node = nodeset->nodeTab[i-1];
    // xmlNodeSetContent (node, text);
    // xmlUnsetProp (xmlNodePtr node, const xmlChar * name)
    xmlUnlinkNode (node);
    xmlFreeNode (node);
    nodeset->nodeTab[i-1] = NULL;
  }
  xmlXPathFreeObject (xpathObjPtr);
}


/**
 * Add a (XML) attribute to the document.
 *
 * @param operation the operation to run (contains the attribute
 *        to add)
 * @param document the output document
 */
static void
process_attribute_add (const struct EBICS_MSG_Spec *operation,
                       struct EBICS_genex_document *document)
{

  int i;
  xmlNodePtr node;
  xmlXPathContextPtr xpathCtxPtr = document->xpath;

  xmlChar *text = operation->data.attribute.value;
  xmlChar *name = operation->data.attribute.name;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression
    (BAD_CAST operation->xpath,
     xpathCtxPtr);
  assert (NULL != xpathObjPtr);
  assert (NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG (EBICS_LOGLEVEL_INFO,
       "process_attribute_set: \
       xpath: %s Xpathresult: %p, \
       nodeset: %p, nodeNr: %u", 
       operation->xpath,
       xpathObjPtr, 
       nodeset,
       nodeset->nodeNr);

  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    node = nodeset->nodeTab[i-1];
    // xmlNodeSetContent (node, text);
    // xmlUnsetProp (xmlNodePtr node, const xmlChar * name)
    xmlUnlinkNode (node);
    xmlFreeNode (node);
    nodeset->nodeTab[i-1] = NULL;
  }
  xmlXPathFreeObject (xpathObjPtr);
  free (name);
  free (text);
}

/**
 * Implement a add node operation.
 *
 * @param operation the operation to run (contains the new node)
 * @param document the output document
 */
static void
process_add_node (const struct EBICS_MSG_Spec *operation,
                  struct EBICS_genex_document *document)
{
  int i;
  xmlNodePtr node;

  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression
    (BAD_CAST operation->xpath,
     xpathCtxPtr);

  assert (NULL != xpathObjPtr);
  assert (NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG (EBICS_LOGLEVEL_INFO,
       "add_node: xpath: %s Xpathresult: %p, \
       nodeset: %p, nodeNr: %u", 
       operation->xpath,
       xpathObjPtr, 
       nodeset,
       nodeset->nodeNr);

  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    node = nodeset->nodeTab[i-1];
    xmlNodePtr new = xmlCopyNodeList
      (operation->data.add_node.node);
    assert (NULL != new);
    xmlNodePtr res = xmlAddChild (node, new);
    assert (NULL != res);
    assert (res == new);
  }
  xmlXPathFreeObject (xpathObjPtr);
}

/**
 * Implement the delete operation.
 *
 * @param operation the delete operation to run
 * @param document output
 */
static void
process_del_node (const struct EBICS_MSG_Spec *operation,
                  struct EBICS_genex_document *document)
{

  int i;
  xmlNodePtr node;
  xmlXPathContextPtr xpathCtxPtr = document->xpath;
  xmlXPathObjectPtr xpathObjPtr = xmlXPathEvalExpression
    (BAD_CAST operation->xpath,
     xpathCtxPtr);

  assert(NULL != xpathObjPtr);
  assert(NULL != xpathObjPtr->nodesetval);
  xmlNodeSetPtr nodeset = xpathObjPtr->nodesetval;

  LOG(EBICS_LOGLEVEL_INFO,
      "del_node: xpath: %s Xpathresult: %p, \
      nodeset: %p, nodeNr: %u", 
      operation->xpath,
      xpathObjPtr, 
      nodeset, nodeset->nodeNr);

  LOG (EBICS_LOGLEVEL_INFO,
       "del_nodes: Number of Nodes found: %d\n",
       nodeset->nodeNr);

  for (i = (nodeset->nodeNr); i > 0; i--)
  {
    node = nodeset->nodeTab[i-1];
    LOG (EBICS_LOGLEVEL_INFO,
         "\tFound: %s",
         node->name);
    xmlUnlinkNode (node);
    xmlFreeNode (node);
    nodeset->nodeTab[i-1] = NULL;
  }
  xmlXPathFreeObject (xpathObjPtr);
}

/**
 * Process a "subcommand" operation.
 *
 * @param operation the subcommand operation to run
 * @param document the output document.
 */
static void
process_subcommand (const struct EBICS_MSG_Spec *operation,
                    struct EBICS_genex_document *document)
{
  operation->data.subcommand.function
    (operation->data.subcommand.cls,
     document);
}

/**
 * Parse a end operation.
 *
 * @param operation operation object.
 * @param document output document.
 */
static void
process_end (const struct EBICS_MSG_Spec *operation,
             struct EBICS_genex_document *document)
{
  remove_lfts_optional_nodes (document);
  remove_any_nodes (document);
}

/**
 * Parse document according to given command specification.
 * Each element of the operation list is called with the
 * appropriate parser function.
 *
 * @param ops Array containing MSG_spec structs.
 * @param document Pointer to the base document
 */
void
EBICS_MSG_parse_spec (const struct EBICS_MSG_Spec *ops,
                      struct EBICS_genex_document *document)
{
  uint8_t index;
  const struct EBICS_MSG_Spec *op;

  for (index = 0;
       EBICS_MSG_OP_END != ops[index].operation;
       index++)
  {
    /* TODO What if EBICS_MSG_OP_END is not at end of list? */

    op = &ops[index];
    switch (op->operation)
    {
      case EBICS_MSG_OP_SELECT_CHOICE:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'select_choice'");
        process_choice (op, document);
        break;
      case EBICS_MSG_OP_UNIQUE_CHOICE:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'unique_choice'");
        process_unique (op, document);
        break;
      case EBICS_MSG_OP_SET_UINT:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'set_uint'");
        process_uint (op, document);
        break;
      case EBICS_MSG_OP_SET_INT:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'set_int'");
        process_int (op, document);
        break;
      case EBICS_MSG_OP_SET_FLAG:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'set_flag'");
        process_flag (op, document);
        break;
      case EBICS_MSG_OP_SET_STRING:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'set_string'");
        process_string (op, document);
        break;
      case EBICS_MSG_OP_SET_DATE:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'set_date'");
        process_date (op, document);
        break;
      case EBICS_MSG_OP_SET_ATTRIBUTE:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'set_attribute'");
        process_attribute_set (op, document);
        break;
      case EBICS_MSG_OP_ADD_ATTRIBUTE:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'set_attribute'");
        process_attribute_add (op, document);
        break;
      case EBICS_MSG_OP_DEL_ATTRIBUTE:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'set_attribute'");
        process_attribute_del (op, document);
        break;
      case EBICS_MSG_OP_ADD_NODE:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'add_node'");
        process_add_node (op, document);
        break;
      case EBICS_MSG_OP_DEL_NODE:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'del_node'");
        process_del_node (op, document);
        break;
      case EBICS_MSG_OP_SUBCOMMAND:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'subcommand'");
        process_subcommand (op, document);
        break;
      case EBICS_MSG_OP_CLEAN:
        LOG (EBICS_LOGLEVEL_INFO,
             "parse_spec: executing 'add_node'");
        process_end (op, document);
        break;

      default:
        /*TODO should this be the error case? */
        LOG (EBICS_LOGLEVEL_WARNING,
             "Default case hit, what to do?\n");
    }
  }
}
