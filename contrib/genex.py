#!/usr/bin/python2.7
"""
  This file is part of libfints

  libfints is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  libfints is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  libfints; see the file COPYING.  If not, If not, see <http://www.gnu.org/license>
"""

"""
    This tool transforms the FinTS xsd schema files to an intermediate format
    which is easier to process and smaller than the full schema files.
    It accepts a base schema file and a base element that is used as the root element
    for generating the intermediate tree. Additionaly you can supply a list of
    leaf elements, these are not further expanded while generating the structures.
"""


import sys
from lxml import etree as ET
from urlparse import urljoin

def merge_dicts(*dict_args):
    '''
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    '''
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

def ltag(x):
    """Return local name of Element 
    """
    if type(x) is not ET._Element:
        return None
    return ET.QName(x).localname

def split_qualifier(nsmap, qname, target=None):
    """Split QName to Namespace and Identifier.
    Return: Tuple with Namespace and Identifier
    """
    if qname.startswith('{'):
        namespace, identifier = qname[1:].split('}')
    elif ':' in qname:
        namespace, identifier = qname.split(":")
        namespace = nsmap[namespace]
    elif target != None:
        namespace = target
        identifier = qname
    else:
        namespace = None
        identifier = qname
        namespace = nsmap[namespace]

    return namespace, identifier

def to_qname(namespace, identifier):
    """Return QName built from namespace and identifier
    """
    return "{%s}%s"%(namespace, identifier)

def ns_uri_to_prefix(uri, nsmap):
    return nsmap.keys()[nsmap.values().index(uri)]

class Builtin(object):
    def __init__(self, elementtype):
        self.text = elementtype

class SchemaGen(object):
    """Schemagen class for parsing schema files to libfints-choicetree.
    """
    def __init__(self, docloc, leafelements = [], verbosity =  0):
        # xml document where our stuff is located
        if verbosity > 0:
            print "Parsing", docloc
        self.verbosity = verbosity
        self.doc = ET.parse(docloc)
        self.docloc = docloc
        self.ns = {
            "xs": "http://www.w3.org/2001/XMLSchema",
        }

        self.imports = {}
        self.includes = []
        self.leafelements = ["EncryptedData"]
        self.leafelements += leafelements

        self.target_namespace = self.doc.xpath("/xs:schema/@targetNamespace",
                namespaces=self.ns)[0]
        
        if self.verbosity > 0:
            print "NSMAP: ",self.doc.getroot().nsmap

        for imp in self.doc.xpath("/xs:schema/xs:import", namespaces=self.ns):
            if imp.get("namespace") not in self.imports:
                url = urljoin(docloc, imp.get("schemaLocation"))
                self.imports[imp.get("namespace")] = SchemaGen(url, leafelements, self.verbosity)

        for imp in self.doc.xpath("/xs:schema/xs:include", namespaces=self.ns):
            if imp.get("namespace") not in self.imports:
                url = urljoin(docloc, imp.get("schemaLocation"))
                self.includes.append(SchemaGen(url, leafelements, self.verbosity))


    def search_typedef(self, typename):
        """search for a type definition
        return element that contains the (local) definition of the given
        typename
        """
        
        if self.verbosity > 0:
            print "Searching for", typename

        namespace, identifier = split_qualifier(self.doc.getroot().nsmap, typename)
        qname = to_qname(namespace, identifier)

        # Specified type is a builtin type of xml-schema
        if namespace == "http://www.w3.org/2001/XMLSchema":
            return self, Builtin("BUILTIN%s"%identifier)

        # Specified type is in target namespace, search in current documents and includes
        elif namespace == self.target_namespace:

            # Is type of complexType?
            typedef = self.doc.find("./xs:complexType[@name='%s']" % (identifier),
                    self.ns)
            if typedef is not None:
                return (self, typedef)

            # Is type of simpleType?
            typedef = self.doc.find("./xs:simpleType[@name='%s']" % (identifier),
                    self.ns)
            if typedef is not None:
                return (self, typedef)
            
            # Is type of attributeGroup?
            typedef = self.doc.find("./xs:attributeGroup[@name='%s']" % (identifier),
                    self.ns)
            if typedef is not None:
                return (self,typedef)

            # Is type of group?
            typedef = self.doc.find("./xs:group[@name='%s']" % (identifier),
                    self.ns)
            if typedef is not None:
                return (self,typedef)

            # Search for type in included documents
            for inc in self.includes:
                result = inc.search_typedef(qname)
                if result is not None:
                    return result
        
        # Specified type is in foreign namespace, lookup type in imported documents
        elif namespace != self.target_namespace:
            import_doc = self.imports[namespace]
            result = import_doc.search_typedef(qname)
            if result is not None:
                return result

        else:
            # Specified type can not be found
            raise Exception("No typedef for '%s'" % (typename,))


    def search_element(self, elementname):
        """search for a element with type elementtype
        return element that contains the (local) definition of the given
        typename or list thereof if substitutions exist
        """
        namespace,identifier = split_qualifier(
                self.doc.getroot().nsmap, 
                elementname, 
                self.target_namespace)
        qname = to_qname(namespace, identifier)

        resultset = {'original':None, 'substitutions':[]}


        # search in target namespace (local and included)
        if namespace == self.target_namespace:
            result = self.find_element(identifier)[1]
            if len(result) > 0 and result[0] is not None:
                resultset['original'] = result[0]
                prefixed = "%s:%s"%(ns_uri_to_prefix(namespace, self.doc.getroot().nsmap),
                        identifier)
                resultset['substitutions'] += self.find_substitutions(prefixed)[1]
                return (self,resultset)
            
            else:
                for include in self.includes:
                    sg, result = include.search_element(identifier)
                    if result['original'] is not None:
                        resultset['original'] = result['original']
                        prefixed = "%s:%s"%(ns_uri_to_prefix(namespace, 
                            self.doc.getroot().nsmap),
                            identifier)
                        resultset['substitutions'] += sg.find_substitutions(prefixed)[1]
                        return self, resultset
            return self, resultset

        # search in imported namespaces
        elif namespace != self.target_namespace:
            extdoc = self.imports[namespace]
            env, resultset = extdoc.search_element(qname)
            if resultset['original'] is not None:
                return env,resultset

        else:
            raise Exception("No element definition for '%S'"%identifier)

    def find_element(self, name):
        return self,[self.doc.find("./xs:element[@name='%s']"%(name), self.ns)]
    def find_substitutions(self, name):
        return self,self.doc.findall("./xs:element[@substitutionGroup='%s']"%(name), self.ns)
    def find_complex(self, name):
        return self,[self.doc.find("./xs:complexType[@name='%s']"%(name), self.ns)]
    def find_simple(self, name):
        return self,[self.doc.find("./xs:simpleType[@name='%s']"%(name), self.ns)]
    def find_attributegroup(self, name):
        return self,[self.doc.find("./xs:attributeGroup[@name='%s']"%(name), self.ns)]

    def find_imported_element(self, name):
        """Search element in the imported files
        """
        for imported in self.imports:
            result = imported.search_element(name)
            if result is not None:
                return result
        return None

    def find_included_element(self, name):
        """Search element in the included files
        """

    def add_choices(self, element, parent):
        choice = ET.Element("CHOICES")
        parent.append(choice)
        minoccs = element.get("minOccurs")
        if minoccs:
            choice.set("lfts_optional", "True")
        for child in element:
            if type(child) is not ET._Element:
                continue
            position = element.index(child)
            position = ET.Element("CHOICE", index=str(position))
            choice.append(position)
            self.generate_structure(child, position)

    def add_sequence(self, element, parent):
        for child in element:
            if type(child) is not ET._Element:
                continue
            self.generate_structure(child, parent)

    def add_attribute(self, element, parent):
        attribute = element.get("name")
        if attribute is not None:
            if element.get("fixed"):
                parent.set(attribute, element.get("fixed"))
            else:
                parent.set(attribute, "PLACEHOLDER")

    def add_any(self, element, parent):
        child = ET.Element("ANY")
        parent.append(child)

    def add_element(self, element, parent):
        if self.verbosity > 0:
            if element.get("name"):
                print("Adding element: {%s}%s"%(self.target_namespace,element.get("name")))
            elif element.get("ref"):
                print("Adding element: NS: %s, REF: %s"%(self.target_namespace,element.get("ref")))
        if element.get("name") in self.leafelements:
            if self.verbosity > 0:
                print "Ignoring leafelement: ", element.get("name")
            return
        
        if element.get("type") is not None:
            element_type = element.get("type")
            element_name = element.get("name")
            #print "generating element '%s' from type '%s'" % (element_name, element_type)
            new_element = ET.Element(to_qname(self.target_namespace, element_name), nsmap=self.doc.getroot().nsmap)
            env, element_type = self.search_typedef(element_type)
            env.generate_structure(element_type, new_element)
            minoccs = element.get("minOccurs")
            if minoccs:
                new_element.set("lfts_optional", "True")
            if len(element) == 0:
                parent.append(new_element)
                return
            for child in element:
                self.generate_structure(child, new_element)
            parent.append(new_element)
        elif element.get("ref") is not None:
            raw_ref = element.get("ref")
            ref = to_qname(*split_qualifier(self.doc.getroot().nsmap, raw_ref))
            sg, elements = self.search_element(ref)
            if len(elements['substitutions']) > 0:
                choice = ET.Element("CHOICES")
                parent.append(choice)
                for child in elements['substitutions']:
                    position = elements['substitutions'].index(child)
                    position = ET.Element("CHOICE", index=str(position))
                    choice.append(position)
                    self.generate_structure(child, position)
            else:
                sg.generate_structure(elements['original'], parent)

        elif element.get("abstract") and element.get("abstract").lower() == "true":
            if self.verbosity > 0:
                print("Looking for abstract substitutions for %s"%(element.get("name")))

            print("SKIPPING ABSTRACT ELEMENT")
            
        elif element.get("name") is not None:
            name = element.get("name")
            if self.verbosity > 0:
                print "generating element from name '%s'" % (name,)
            new_element = ET.Element(to_qname(self.target_namespace, name), nsmap=self.doc.getroot().nsmap)
            minoccs = element.get("minOccurs")
            if isinstance(minoccs, str) and int(minoccs) == 0:
                new_element.set("lfts_optional", "True")
            if len(element) == 0:
                parent.append(new_element)
                return
            for child in element:
                self.generate_structure(child, new_element)
            parent.append(new_element)
        else:
            raise Exception("Element could not be handled")

    def add_complexcontent(self, element, parent):
        restrictions = element.xpath("xs:restriction", namespaces=self.ns)
        extensions = element.xpath("xs:extension", namespaces=self.ns)
        assert(len(restrictions) + len(extensions) == 1)
        for extension in extensions:
            content_env, content_base_type = self.search_typedef(extension.get("base"))
            content_env.generate_structure(content_base_type, parent)
            for structure in extension:
                self.generate_structure(structure, parent)
        for restriction in restrictions:
            for structure in restriction:
                self.generate_structure(structure, parent)

    def add_simplecontent(self, element, parent):
        restrictions = element.xpath("xs:restriction", namespaces=self.ns)
        extensions = element.xpath("xs:extension", namespaces=self.ns)
        assert(len(restrictions) + len(extensions) == 1)
        for extension in extensions:
            content_env, content_base_type = self.search_typedef(extension.get("base"))
            content_env.generate_structure(content_base_type, parent)
            for structure in extension:
                self.generate_structure(structure, parent)
        for restriction in restrictions:
            for structure in restriction:
                self.generate_structure(structure, parent)

    def add_simpletype(self, element, parent):
        if self.verbosity > 0:
            print("Adding simpleType %s"%(element.get("name")))
        restriction = element.xpath("./xs:restriction", namespaces=self.ns)
        if restriction is not None and len(restriction) > 0 and self.verbosity > 0:
            print(restriction)
            basetype = restriction[0].get("base")
            parent.text = "simpleType:%s"%basetype
        else:
            parent.text = "PLACEHOLDER"

    def add_complextype(self, element, parent):
        for content in element:
            self.generate_structure(content, parent)

    def add_group(self, element, parent):
        sg = self
        if element.get("ref"):
            sg, element = self.search_typedef(element.get("ref"))
        for content in element:
            self.generate_structure(content, parent)

    def add_attributegroup(self, element, parent):
        sg = self
        if element.get("ref"):
            sg,element = self.search_typedef(element.get("ref"))
        for content in element:
            sg.generate_structure(content, parent)

    def add_builtin(self, element, parent):
        #parent.text = "BUILTIN"
        parent.text = element.text

    def generate_structure(self, structure_def, el):
        """Generate structure for given type
        """
        if self.verbosity > 0:
            if isinstance(structure_def,ET._Element):
                print("Generating: Structure: %s \t Name: %s \t Ref: %s"%(
                    ltag(structure_def), 
                    structure_def.get("name"),
                    structure_def.get("ref")))
            else:
                print "generate_stucture for", ltag(structure_def)

        if isinstance(structure_def,Builtin) and structure_def.text.startswith("BUILTIN"):
            self.add_builtin(structure_def, el)
        elif isinstance(structure_def, ET._Comment):
            if self.verbosity > 0:
                print("Skipping comment")
        elif ltag(structure_def) == "sequence":
            self.add_sequence(structure_def, el)
        elif ltag(structure_def) == "choice":
            self.add_choices(structure_def, el)
        elif ltag(structure_def) == "any":
            self.add_any(structure_def, el)
        elif ltag(structure_def) == "element":
            self.add_element(structure_def, el)
        elif ltag(structure_def) == "complexContent":
            self.add_complexcontent(structure_def, el)
        elif ltag(structure_def) == "simpleContent":
            self.add_simplecontent(structure_def, el)
        elif ltag(structure_def) == "simpleType":
            self.add_simpletype(structure_def, el)
        elif ltag(structure_def) == "complexType":
            self.add_complextype(structure_def, el)
        elif ltag(structure_def) == "group":
            self.add_group(structure_def, el)
        elif ltag(structure_def) == "annotation":
            if self.verbosity > 0:
                print("Skipping annotation")
        elif type(structure_def) == ET._Element and structure_def.get("abstract", "false") == "true":
            if self.verbosity > 0:
                print("Skipping abstract type")
        elif ltag(structure_def) == "attribute":
            self.add_attribute(structure_def, el)
        elif ltag(structure_def) == "anyAttribute":
            self.add_attribute(structure_def, el)
        elif ltag(structure_def) == "attributeGroup":
            self.add_attributegroup(structure_def, el)
        else:
            raise Exception("Unknown content " + str(ltag(structure_def)))


    def generate_libfintstree(self, element_name):
        if self.verbosity > 0:
            print "Root Element: ", element_name

        nsmap = self.doc.getroot().nsmap
        nsmap["ds"] = "http://www.w3.org/2000/09/xmldsig#"
        #nsmap.pop(nsmap.keys()[nsmap.values().index("http://www.w3.org/2001/XMLSchema")])
        el = ET.Element(to_qname(self.target_namespace,element_name), nsmap=nsmap)
        tree = ET.ElementTree(el)
        sg, element_root = self.search_element(element_name)
        for element in element_root['original']:
            if type(element) is ET._Element:
                sg.generate_structure(element, el)
        if element_root['original'].get("type") is not None:
            sg, element_root = self.search_typedef(element_root['original'].get("type"))
            for element in element_root:
                sg.generate_structure(element, el)
        
        return tree

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description="Generate libfints-choice-trees.")
    parser.add_argument("sourcefile", help="Sourcefile with the definition of the element that is to expand.")
    parser.add_argument("sourceelement", help="Root element for structure expansion.")
    parser.add_argument("-l", "--leafelement", default=[], dest="leafes", nargs="+", help="List of leafs that should not be expanded.")
    parser.add_argument("-o", "--output", default="-", dest="output", help="Output for tree (Default: stdout).")
    parser.add_argument("-v", "--verbose", action="count", default=0, help="Verbosity, use multiple times to increase self.verbosity.")

    args = parser.parse_args()
    
    from pprint import pprint

    schema = SchemaGen(args.sourcefile, args.leafes, verbosity=args.verbose)
    lftstree = schema.generate_libfintstree(args.sourceelement)

    lftstree.write(args.output, encoding="utf-8", pretty_print=True, xml_declaration=True)

    exit(0)

