# I'm in the public domain.

AC_PREREQ([2.69])
AC_INIT([libebics], [0.1], [taler-bug@gnunet.org])
AC_CONFIG_SRCDIR([src/libebics.c])
AC_CONFIG_HEADERS([config.h])
AM_INIT_AUTOMAKE([subdir-objects 1.9 tar-pax])

# Checks for programs.
AC_PROG_CC
AC_PROG_CC_C99
LT_INIT

# Checks for libraries.
AC_CONFIG_MACRO_DIR([m4])

# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_SIZE_T
AC_TYPE_UINT64_T
AC_TYPE_UINT8_T

# Checks for library functions.

libxml2=0
AM_PATH_XML2([2.9.1], libxml2=1)
if test $libxml2 = 0
then
  AC_MSG_ERROR([[
***
*** You need libxml2 to build this program.
***]])
fi

# libcurl
libcurl=0
NEED_LIBCURL_VERSION=7.0.0
LIBCURL_CHECK_CONFIG("yes", [NEED_LIBCURL_VERSION], libcurl=1)

if test $libcurl = 0
then
  AC_MSG_ERROR([[
***
*** You need libcurl to build this program.
***]])
fi


# libxmlsec
libxmlsec=0
xmlsec1_config_args="--crypto=gnutls"
NEED_LIBXMLSEC_VERSION="1.2.0"
AM_PATH_XMLSEC1([$NEED_LIBXMLSEC_VERSION], libxmlsec=1)

if test $libxmlsec = 0
then
  AC_MSG_ERROR([[
***
*** You need libxmlsec to build this program.
*** (at least version $NEED_LIBXMLSEC_VERSION 
***  is required.)
***]])
fi


# zlib
zlib=0
EXMPP_ZLIB(zlib=1)

if test $zlib = 0
then
  AC_MSG_ERROR([[
***
*** You need zlib to build this program.
*** (at least version $NEED_zlib_VERSION 
***  is required.)
***]])
fi


# libgcrypt
gcrypt=0
NEED_LIBGCRYPT_API=1
NEED_LIBGCRYPT_VERSION=1.6.3


AM_PATH_LIBGCRYPT("$NEED_LIBGCRYPT_API:$NEED_LIBGCRYPT_VERSION", gcrypt=1)

if test $gcrypt = 0
then
  AC_MSG_ERROR([[
***
*** You need libgcrypt to build this program.
**  This library is for example available at
***   ftp://ftp.gnupg.org/gcrypt/libgcrypt/
*** (at least version $NEED_LIBGCRYPT_VERSION (API $NEED_LIBGCRYPT_API)
***  is required.)
***]])
fi
AC_DEFINE_UNQUOTED([NEED_LIBGCRYPT_VERSION], "$NEED_LIBGCRYPT_VERSION", [required libgcrypt version])

# Check for GNUnet's libgnunetutil.
libgnunetutil=0
AC_MSG_CHECKING([for libgnunetutil])
AC_ARG_WITH(gnunet,
            [AS_HELP_STRING([--with-gnunet=PFX], [base of GNUnet installation])],
            [AC_MSG_RESULT([given as $with_gnunet])],
            [AC_MSG_RESULT(not given)
             with_gnunet=yes])
AS_CASE([$with_gnunet],
        [yes], [],
        [no], [AC_MSG_ERROR([--with-gnunet is required])],
        [LDFLAGS="-L$with_gnunet/lib $LDFLAGS"
         CPPFLAGS="-I$with_gnunet/include $CPPFLAGS"])
AC_CHECK_HEADERS([gnunet/platform.h gnunet/gnunet_util_lib.h],
 [AC_CHECK_LIB([gnunetutil], [GNUNET_SCHEDULER_run], libgnunetutil=1)],
  [], [#ifdef HAVE_GNUNET_PLATFORM_H
        #include <gnunet/platform.h>
       #endif])
AS_IF([test $libgnunetutil != 1],
  [AC_MSG_ERROR([[
***
*** You need libgnunetutil to build this program.
*** This library is part of GNUnet, available at
***   https://gnunet.org
*** ]])])




dnl ==========================================================================
dnl See if we can find GnuTLS
dnl ==========================================================================
GNUTLS_CONFIG="libgnutls-config"
GNUTLS_MIN_VERSION="3.4.0"
GNUTLS_VERSION=""
GNUTLS_CFLAGS=""
GNUTLS_LIBS=""
GNUTLS_FOUND="no"

PKG_CHECK_MODULES(GNUTLS, gnutls >= $GNUTLS_MIN_VERSION,
    [GNUTLS_FOUND=yes],
    [GNUTLS_FOUND=no])
if test "z$GNUTLS_FOUND" = "zno" ; then 
    PKG_CHECK_MODULES(GNUTLS, libgnutls >= $GNUTLS_MIN_VERSION,
        [GNUTLS_FOUND=yes],
        [GNUTLS_FOUND=no])
fi

if test "z$GNUTLS_FOUND" = "zno" ; then
    AC_MSG_CHECKING(for gnutls libraries >= $GNUTLS_MIN_VERSION) 
    if test "z$with_gnutls" != "z" ; then
        GNUTLS_CONFIG=$with_gnutls/bin/$GNUTLS_CONFIG
    fi
    if ! $GNUTLS_CONFIG --version > /dev/null 2>&1 ; then
        if test "z$with_gnutls" != "z" ; then
            AC_MSG_ERROR(Unable to find gnutls at '$with_gnutls')
        else
            AC_MSG_RESULT(no)
        fi
    else
        vers=`$GNUTLS_CONFIG --version | awk -F. '{ printf "%d", ($1 * 1000 + $2) * 1000 + $3;}'`
        minvers=`echo $GNUTLS_MIN_VERSION | awk -F. '{ printf "%d", ($1 * 1000 + $2) * 1000 + $3;}'`
        if test "$vers" -ge "$minvers" ; then
            GNUTLS_CFLAGS="$GNUTLS_CFLAGS `$GNUTLS_CONFIG --cflags`"
            GNUTLS_LIBS="$GNUTLS_LIBS `$GNUTLS_CONFIG --libs`"
            GNUTLS_FOUND=yes
        else
            AC_MSG_ERROR(You need at least gnutls $GNUTLS_MIN_VERSION for this version of $XMLSEC_PACKAGE)
        fi      
    fi
fi

AC_SUBST(GNUTLS_CFLAGS)
AC_SUBST(GNUTLS_LIBS)
AC_SUBST(GNUTLS_MIN_VERSION)

CPPFLAGS="$XML_CPPFLAGS $CPPFLAGS $LIBCURL $LIBCURL_CPPFLAGS $XMLSEC1_CFLAGS $LIBGNUTLS_CFLAGS $ZLIB_CFLAGS"

AC_CONFIG_FILES([Makefile
                 m4/Makefile
                 contrib/Makefile
                 src/Makefile
                 src/include/Makefile
                 src/tests_xmlproto/Makefile])
AC_OUTPUT
